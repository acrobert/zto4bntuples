import os
import pickle
import numpy as np
import math
import time
#import torch
#import torch_geometric as geo
import random
import yaml

class timer:
    def __init__(self):
        self.start = time.time()

    def time(self):
        return time.time() - self.start

def xglob_for_ftype(date, run, dset, runname, ftype):

    datasets = { 2: {'dp': 'DiPhotonJets_MGG-80toInf_13TeV_amcatnloFXFX_pythia8',
                     'gj1': 'GJet_Pt-20to40_DoubleEMEnriched_MGG-80toInf_TuneCP5_13TeV_Pythia8',
                     'gj2': 'GJet_Pt-40toInf_DoubleEMEnriched_MGG-80toInf_TuneCP5_13TeV_Pythia8',
                     'gj3': 'GJet_Pt-20toInf_DoubleEMEnriched_MGG-40to80_TuneCP5_13TeV_Pythia8'},
                 3: {'dp1': 'DoublePhoton_FlatPt-5To500_13p6TeV',
                     'dp2': 'DoublePhoton_FlatPt-500To1000_13p6TeV',
                     'dp3': 'DoublePhoton_FlatPt-1000To1500_13p6TeV',
                     'dp4': 'DoublePhoton_FlatPt-1500To3000_13p6TeV',
                     'dp5': 'DoublePhoton_FlatPt-3000To4000_13p6TeV',
                     'dp6': 'DoublePhoton_FlatPt-4000To5000_13p6TeV',
                     'gj1': 'GJet_Pt-10to40_DoubleEMEnriched_TuneCP5_13p6TeV_pythia8',
                     'gj2': 'GJet_Pt-40toInf_DoubleEMEnriched_TuneCP5_13p6TeV_pythia8' } }

    for i in range(1, 18):
        datasets[2]['dt'+str(i)] = 'DoubleEG'

    t3 = 'root://cmsdata.phys.cmu.edu'

    cmd = 'xrdfs {} ls /store/user/acrobert/egiddata/{}/{}/{}'.format(t3, date, datasets[run][dset], runname)
    os.system(cmd +' > tmpf')
    out = open('tmpf', 'r').read().split('\n')
    #out = subprocess.run(cmd, stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')                                                                                                                

    while not check_for_ftype(out, ftype):

        new_out = []
        for fld in out:
            if fld != '':
                #cmd = ['xrdfs', t3, 'ls', fld]                                                                                                                                                          
                #new_out += subprocess.run(cmd, stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')                                                                                               
                cmd = 'xrdfs {} ls {}'.format(t3, fld)
                os.system(cmd + ' > tmpf')
                new_out += open('tmpf', 'r').read().split('\n')

        out = new_out

    ret = []
    for f in out:
        if '.root' in f:
            ret.append(f)

    return ret

def xglob_onedir(path):

    #nanopath = '/store/user/acrobert/Znano/{}'.format(date)
    t3 = 'root://cmsdata.phys.cmu.edu/'

    cmd = 'xrdfs {} ls {}'.format(t3, path)
    os.system(cmd +' > tmpf')
    out = open('tmpf', 'r').read().split('\n')
    #out = subprocess.run(cmd, stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')                                                                                                                

    return out

def check_for_ftype(lst, ftype):

    bl = False
    for l in lst:
        if ftype in l:
            bl = True

    return bl

def calc_xs(dataset):

    os.system('curl https://raw.githubusercontent.com/cms-sw/genproductions/master/Utilities/calculateXSectionAndFilterEfficiency/genXsec_cfg.py -o ana.py > /dev/null 2>&1')
    os.system('dasgoclient -query="file dataset={}" > dastmp.txt 2>&1'.format(dataset))

    with open('dastmp.txt','r') as dasout:
        daslines = dasout.readlines()
        print('N files: {}'.format(len(daslines)-1))
        xsarr = []
        errarr = []
        
        for i in range(min(10, len(daslines)-1)):
            thisfile = daslines[i].strip('\n')
            print('file: {}'.format(thisfile))
            os.system('cmsRun ana.py inputFiles="file:root://cms-xrd-global.cern.ch/{}" maxEvents=-1 > xstmp.txt 2>&1'.format(thisfile))

            xs = -1.

            with open('xstmp.txt','r') as xsout:
                xslines = xsout.readlines()
                
                for j in range(len(xslines)):
                    if 'After filter: final cross section' in xslines[j]:
                        eles = xslines[j].strip('\n').split(' ')
                        xsarr.append(float(eles[6]))
                        errarr.append(float(eles[8]))

    xsarr = np.array(xsarr)
    print('dataset xs:', xsarr.mean(), xsarr.std())
    os.system('rm dastmp.txt && rm xstmp.txt')
