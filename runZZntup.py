import os

cfg='Zto4Bntuples/ntuples/python/ntuplizer_cfg.py'

# MINIAOD
#inputFiles_ = '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v1/2430000/086B8995-7BC6-E14F-9033-56E0675E9037.root'
#inputFiles_ = '/store/mc/RunIISummer20UL18MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/40000/018BD1A4-4C9A-A247-BD30-71DB1A95B7C8.root'
#inputFiles_ = '/store/mc/RunIISummer20UL18MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/40000/01BF27C6-3790-BA49-9AB6-EBC044769933.root'
#inputFiles_ = '/store/mc/RunIISummer20UL18MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/40000/021883C4-65D7-D54A-A353-F900007CB34E.root'
#inputFiles_ = '/store/mc/RunIISummer20UL18MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/40000/02518853-F5A3-5B42-8806-1EB9CE072DE4.root'
#inputFiles_ = '/store/mc/RunIISummer20UL18MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/40000/02C32A45-C597-F545-AEAE-B8C93A12607A.root'
#inputFiles_ = '/store/mc/RunIISummer20UL18MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/40000/03A2170F-5F1A-8B45-B903-6E2E68090E9C.root'

inputFiles_ = [
    #'/store/mc/RunIISummer20UL17MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v1/2430000/17AE77F9-0A76-0A48-81EB-4B9538EC7E3D.root'
    #, '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v1/2430000/1CC563B6-F69C-3946-B070-035FBF882FBC.root'
    #, '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v1/2430000/1F0B102C-C417-F54A-83E1-61016EB0DC1D.root'
    #, '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v1/2430000/2DB1ECBE-DF07-2940-A984-CE49EC586633.root'
    #, '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v1/2430000/38BA3A84-9886-3344-AEC1-A1FB1E6927A3.root'
    #, '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v1/2520000/093C9E2B-8D11-A548-923A-FFF814E84B30.root'
    #, '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v1/2520000/0F23AF9E-13A1-0548-8F9C-E9FB8D57D278.root'
    #, '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v1/2520000/1EF249E9-27B4-7845-AB06-131B76717F93.root'

    '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/260000/DD8A3386-0A33-DC47-ACCB-50A7E90EB020.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/01837C49-52F9-C144-9C82-965B88755763.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/EB945EE1-3418-524D-B3AA-37339F23376E.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/1F5394B2-A9AE-7A41-BACA-4E843954EA9B.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/6080BEAB-5B56-394C-9415-950EF316CCBE.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/F6936B20-5658-6745-B5EF-8B8593548C98.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/B1D6FB74-2DE4-AA4A-90CB-F1CF9108B975.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/0CAB0078-6DDA-4A40-ACA6-6894F12F3E5C.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/D628B595-238C-9C49-B442-82F0B06B018C.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/5F9CB12E-A081-DF48-A6DD-5B8E63892D53.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/7A81A4AE-A329-C248-80B1-967C37B62390.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/4AAD06A9-7A49-4C4C-A6A8-BE0681D772B0.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/CF63FA5A-1633-344E-BBCA-D32FC8592A63.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/C694D597-B695-184E-92A0-8F6719DA22B0.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/6F467907-7A22-7549-AAEE-F7FFCF1C19F2.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/6D9A1133-649B-B747-8B21-74B7CA38577A.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/A8700833-93D6-6D4B-B677-19B0488344D4.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/FC5C5594-54EE-774A-A803-01E31AFC38E9.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/1A12126F-597C-1345-B15F-3810F65B477F.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/FEAC893B-A5D0-1343-B659-C24CB01F0A4E.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/4859BA7A-2DDF-EA4A-BC5C-24A38B84FB24.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/521BD58E-E02F-644B-B5BD-1BEF3BB9DF6B.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/524A2E1A-C8D8-1046-BC74-EE43C945DAA2.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/429BFF47-1D77-C740-905D-B068AFACBC26.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/B059751B-4C4B-8648-9EE7-9A150553E26D.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/494F527E-1C6D-E542-8202-79F416FF7F3C.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/A9A19B60-1649-2540-8FFC-D70EDA2902D5.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/0B63B666-F976-8A40-B4BE-67EB4460E895.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/B090A52F-1479-9740-BDEB-BCC7DC7E6DAD.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/CD807CA4-C6F6-AA4D-8DD9-2418712C5BDC.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/197E70D5-0891-DA4B-8D8D-FF1E1DC5DD37.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/2530000/A70D81DE-C864-C746-B74B-872300DA4843.root'
    

]

# AOD
#inputFiles_ = '/store/mc/RunIISummer20UL17RECO/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/AODSIM/106X_mc2017_realistic_v6-v1/2520000/000E2D11-E084-CF41-8315-AAD7414F1DCF.root'

#maxEvents_=-1
maxEvents_=5000
skipEvents_=0
instr = ' '.join([ 'inputFiles='+s for s in inputFiles_ ])
cmd="cmsRun %s %s maxEvents=%d skipEvents=%d"%(cfg,instr,maxEvents_,skipEvents_)
print ('%s'%cmd)
os.system(cmd)

