import os
import numpy as np
#from utils import *
from datadicts import *
import sys
#from spawnCondor import spawnCondor

#cmd = 'cmsDriver.py test_nanoTuples_mc2018 -n 10000000 --mc --eventcontent NANOAODSIM --datatier NANOAODSIM --conditions 106X_upgrade2018_realistic_v15_L1v1 --step NANO --nThreads 1 --era Run2_2018,run2_nanoAOD_106Xv1 --customise PhysicsTools/NanoTuples/nanoTuples_cff.nanoTuples_customizeMC --filein {} --fileout file:nano_ParT_{}.root --customise_commands "process.options.wantSummary = cms.untracked.bool(True)"'
cmd = "cmsRun Zto4Bntuples/ntuples/python/ntuplizer_cfg.py inputFiles={} outputFile={} maxEvents=-1 skipEvents=0"#.format(instr)
#cmd = "cmsRun Zto4Bntuples/ntuples/python/ntuplizer_cfg.py inputFiles={} outputFile={} maxEvents=10000 skipEvents=0"#.format(instr)
checkxs = True

key = sys.argv[1] #'ZZTo4B01j_mc2017UL'
if key in datasets.keys():
    dataset = datasets[key]
else:
    dataset = key
friendtype = 'ZBQ-EPid'

#os.system('. ./mktar.sh > /dev/null 2>&1')
os.system('curl https://raw.githubusercontent.com/cms-sw/genproductions/master/Utilities/calculateXSectionAndFilterEfficiency/genXsec_cfg.py -o ana.py > /dev/null 2>&1')
#os.system('dasgoclient -query="file dataset={}" > dastmp.txt 2>&1'.format(dataset))

if 'j-g' in key:
    os.system('dasgoclient -query="file dataset={} instance=prod/phys03" > dastmp.txt 2>&1'.format(dataset))
else:
    os.system('dasgoclient -query="file dataset={}" > dastmp.txt 2>&1'.format(dataset))


with open('dastmp.txt','r') as dasout:
    daslines = dasout.readlines()
    print('N files: {}'.format(len(daslines)-1))
    xsarr = []
    errarr = []

    for i in range(len(daslines)):
        thisfile = daslines[i].strip('\n')

        if 'j-g' not in key:
            os.system('dasgoclient -query="site file={} instance=prod/phys03" > sitetmp.txt 2>&1'.format(thisfile))
            run = False
            with open('sitetmp.txt','r') as siteout:
                sitelines = siteout.readlines()
                
                for line in sitelines:
                    print(line.strip('\n'))
                    if 'DISK' in line or 'Disk' in line:
                        run = True

            os.system('rm sitetmp.txt')
            if not run:
                print(i),
                continue

            if checkxs:
                os.system('cmsRun ana.py inputFiles="file:root://cms-xrd-global.cern.ch/{}" maxEvents=-1 > xstmp.txt 2>&1'.format(thisfile))
                
                xs = -1.
                
                with open('xstmp.txt','r') as xsout:
                    xslines = xsout.readlines()
                    #print(len(xslines))

                    for j in range(len(xslines)):
                        print(xslines[j])
                        if 'After filter: final cross section' in xslines[j]:
                            eles = xslines[j].strip('\n').split(' ')
                            xsarr.append(float(eles[6]))
                            errarr.append(float(eles[8]))
                            print('xs',i,float(eles[6]))
                            xs = float(eles[6])
                            if len(xsarr) >= 5:
                                checkxs = False
                os.system('rm xstmp.txt')
            if not checkxs:
                break

        else:
            if checkxs:
                os.system('cmsRun ana.py inputFiles="file:root://cmsdata.phys.cmu.edu/{}" maxEvents=-1 > xstmp.txt 2>&1'.format(thisfile))
            
                xs = -1.
            
                with open('xstmp.txt','r') as xsout:
                    xslines = xsout.readlines()
                    #print(len(xslines))                                                                                                                                                                           

                    for j in range(len(xslines)):
                        if 'After filter: final cross section' in xslines[j]:
                            eles = xslines[j].strip('\n').split(' ')
                            xsarr.append(float(eles[6]))
                            errarr.append(float(eles[8]))
                            print('xs',i,float(eles[6]))
                            xs = float(eles[6])
                            if len(xsarr) >= 5:
                                checkxs = False
                os.system('rm xstmp.txt')
            if not checkxs:
                break



    xsarr = np.array(xsarr)
    print(xsarr.mean(), xsarr.std())

os.system('rm dastmp.txt')
