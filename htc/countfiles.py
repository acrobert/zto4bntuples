from datadicts import *

key = 'WJets_HT800_up20UL18'

thesefiles = xglob_onedir('/store/user/acrobert/Znano/Jan2023/'+key+'/')

nanos = []
friends = []
for f in thesefiles:
    if 'nano_ParT' in f:
        
        ending = f.split('_')[-1]
        ending = ending.strip('.root')
        ending = ending.strip('file')

        nanos.append(int(ending))

    if 'EPid-Map' in f:

        ending = f.split('_')[-1]
        ending = ending.strip('.root')
        ending = ending.strip('file')

        friends.append(int(ending))
        
nanos.sort()
friends.sort()

both = []
nanosonly = []

for i in range(len(nanos)):

    if nanos[i] in friends:

        both.append(nanos[i])

    else:

        nanosonly.append(nanos[i])

friendsonly = []

for i in range(len(friends)):

    if friends[i] not in nanos:

        friendsonly.append(friends[i])

print(len(nanos), len(friends))
print(len(both))
print(len(nanosonly), len(friendsonly))

print('nanos', nanosonly)

print('friends', friendsonly)

