dasgoclient --query="file=/store/mc/RunIISummer20UL18MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/120000/4E7C6F3B-8478-EE4B-84EF-C035CD5D2A99.root | grep file.nevents" > dsetnevents_crab.txt
NEVENTS=0
while read line;
do
    if [ "$line" != "" ];
    then
        NEVENTS=$line;
    fi;
done < <(cat dsetnevents_crab.txt)
echo $NEVENTS
