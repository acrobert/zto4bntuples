#!/bin/sh

CMSSWpath=/uscms/home/acrobert/nobackup/zto4b
localPathToTarball=/uscms/home/acrobert/nobackup/zto4b/CMSSW_10_6_30/src/
CMSSW=CMSSW_10_6_30

rm -rf ${localPathToTarball}/${CMSSW}.tgz
cd ${CMSSWpath}

tar -zcvf ${localPathToTarball}/${CMSSW}.tgz ${CMSSW} --exclude="*root" --exclude="*npy" --exclude="*.pdf" --exclude="*.jdl" --exclude="*.stdout" --exclude="*.stderr" --exclude="*.log" --exclude="*.png" --exclude="*.dat" --exclude="*.tgz" --exclude-vcs --exclude-caches-all --exclude="${CMSSW}/src/htc"

ls ${localPathToTarball} -alh
xrdfs root://cmseos.fnal.gov/ mkdir /store/user/acrobert/condor
xrdcp -f ${localPathToTarball}/${CMSSW}.tgz root://cmseos.fnal.gov//store/user/acrobert/condor/${CMSSW}.tgz
xrdfs root://cmseos.fnal.gov/ rm /store/user/acrobert/x509up_u57074
xrdcp /uscms/home/acrobert/x509up_u57074 root://cmseos.fnal.gov//store/user/acrobert/.
cd -
