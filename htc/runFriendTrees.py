import os
import numpy as np
from datadicts import datasets, t3dirs
import sys

checkxs = False

key = sys.argv[1]#'ZHToAATo4B_mc2017UL_m30'#'ZJets_HT_600to800_mc2017UL'#'ZGammaToJJ_mc2017UL' #'WZToLNuQQ_mc2017UL'#'ZJets_HT_800toInf_mc2017UL' #'ZZTo2Q2L_mc2017UL'#'ZZTo4B01j_mc2017UL'
dataset = datasets[key]
#friendtype = 'MapEcalPFCs'
#friendtype = 'ZBQ-EPid'
friendtype = 'EPid-Map'

if len(sys.argv) == 4:
    ntorun = int(sys.argv[3])
    start = int(sys.argv[2])
elif len(sys.argv) == 2:
    ntorun = 1000
    start = 0
elif len(sys.argv) > 4:
    #idxtorun = [ int(run[:-1]) for run in sys.argv[2:]]
    idxtorun = [ int(run) for run in sys.argv[2:]] 
    ntorun = 1000




try:
    dir = t3dirs[key]
except KeyError:
    dir = key

def spawnCondor(infile, outfile, evttype, submit=False):
    logname = outfile.strip('.root')

    exctbl = 'skimFriend.sh' if 'j-g' not in evttype else 'fullFriend.sh'

    subcondor_template = """
Executable            = {}
Arguments             = {} {} {}
Should_transfer_files = YES
x509userproxy         = /uscms/home/acrobert/x509up_u57074
Use_x509userproxy     = true
Output                = /uscms/home/acrobert/nobackup/zto4b/CMSSW_10_6_30/src/htc/friendEPid-Map/outs/{}
Error                 = /uscms/home/acrobert/nobackup/zto4b/CMSSW_10_6_30/src/htc/friendEPid-Map/errs/{}
Log                   = /uscms/home/acrobert/nobackup/zto4b/CMSSW_10_6_30/src/htc/friendEPid-Map/logs/{}
Queue
""".format(exctbl, infile, outfile, evttype, logname, logname, logname)

    with open('subcondorFriend','a') as sub:
        sub.write(subcondor_template)
        
    if submit:
        os.system('condor_submit subcondorFriend')

os.system('. ./mktar.sh > /dev/null 2>&1')
os.system('curl https://raw.githubusercontent.com/cms-sw/genproductions/master/Utilities/calculateXSectionAndFilterEfficiency/genXsec_cfg.py -o ana.py > /dev/null 2>&1')
if 'j-g' in key:
    os.system('dasgoclient -query="file dataset={} instance=prod/phys03" > dastmp.txt 2>&1'.format(dataset))
else:
    os.system('dasgoclient -query="file dataset={}" > dastmp.txt 2>&1'.format(dataset))

#exit()

nfiles = 0
with open('dastmp.txt','r') as dasout:
    daslines = dasout.readlines()
    print('N files: {}'.format(len(daslines)))
    daslines.sort()
    xsarr = []
    errarr = []

    #print('Skipping jobs:'),
    #lim = min(len(daslines), start+ntorun) if key != 'TTJets_up20UL18' else len(daslines)
    #lim = min(len(daslines), start+ntorun)

    if idxtorun == []:
        lim = min(len(daslines), start+ntorun)# if key != 'TTJets_up20UL18' else len(daslines) 
        iterr = range(start, lim)
    else:
        iterr = idxtorun

    for i in iterr:
    #for i in range(start, lim):
        thisfile = daslines[i].strip('\n')
        if 'j-g' in dataset:
            fileind = thisfile.split('_')[-1].split('.root')[-2]
        else:
            fileind = i
            
        if key == 'TTJets_up20UL18':
            os.system('dasgoclient -query="site file={}" > sitetmp{}.txt 2>&1'.format(thisfile, fileind))
            check1 = False
            try:
                with open('sitetmp{}.txt'.format(fileind),'r') as siteout:
                    sitelines = siteout.readlines()
                    for line in sitelines:
                        if 'T1_US_FNAL_Disk' in line:
                            check1 = True

                os.system('rm sitetmp{}.txt'.format(fileind))
            except IOError:
                pass

            if not check1:
                continue

            check2 = False
            os.system('dasgoclient -query="file={} | grep file.nevents" > evtstmp{}.txt 2>&1'.format(thisfile, fileind))
            try:
                with open('evtstmp{}.txt'.format(fileind),'r') as siteout:
                    sitelines = siteout.readlines()
                    for line in sitelines:
                        try:
                            if int(line.strip('\n')) <= 70000:
                                check2 = True
                        except ValueError:
                            pass

                os.system('rm evtstmp{}.txt'.format(fileind))
            except IOError:
                pass
            
            if not check2:
                continue


        if checkxs:
            os.system('cmsRun ana.py inputFiles="file:root://cms-xrd-global.cern.ch/{}" maxEvents=-1 > xstmp.txt 2>&1'.format(thisfile))

            xs = -1.

            with open('xstmp.txt','r') as xsout:
                xslines = xsout.readlines()
                #print(len(xslines))

                for j in range(len(xslines)):
                    if 'After filter: final cross section' in xslines[j]:
                        eles = xslines[j].strip('\n').split(' ')
                        xsarr.append(float(eles[6]))
                        errarr.append(float(eles[8]))
                        print('xs',i,float(eles[6]))
                        xs = float(eles[6])
                        if len(xsarr) >= 10:
                            checkxs = False
            os.system('rm xstmp.txt')
            
        if key == 'TTJets_up20UL18':
            print('Adding file {} to queue'.format(fileind))
        if 'j-g' in key:
            addtag = ''
        else:
            addtag = '_skimZJPS1'

        outname = 'friend_'+key+addtag+'_'+friendtype+'_file'+str(fileind)+'.root'
        spawnCondor(thisfile, outname, dir)
        nfiles += 1
        if nfiles >= ntorun:
            break

        
    #print()

    print('Spawning {} condor jobs...'.format(nfiles))
    os.system('condor_submit subcondorFriend')
    print('Submitted!')

    if checkxs:
        xsarr = np.array(xsarr)
        print(xsarr.mean(), xsarr.std())

os.system('rm dastmp.txt && rm subcondorFriend')
