#!/bin/sh
echo "Starting job on " `date` #Date/time of start of job
echo "Running on: `uname -a`" #Condor job is running on this node
echo "System software: `cat /etc/redhat-release`" #Operating System on that node

infile=$1
outfile=$2
evttype=$3
CMSSW=CMSSW_10_6_30

xrdcp -s root://cmseos.fnal.gov//store/user/acrobert/condor/${CMSSW}.tgz .
source /cvmfs/cms.cern.ch/cmsset_default.sh 
tar -xzvf ${CMSSW}.tgz
rm ${CMSSW}.tgz
cd ${CMSSW}/src/

scramv1 b ProjectRename # this handles linking the already compiled code - do NOT recompile
eval `scramv1 runtime -sh` # cmsenv is an alias not on the workers
echo $CMSSW_BASE "is the CMSSW we have on the local worker node"

#cmsDriver.py test_nanoTuples_mc2018 -n 1000000000 --mc --eventcontent NANOAODSIM --datatier NANOAODSIM --conditions ${conditions} --step NANO --nThreads 1 --era Run2_2018,run2_nanoAOD_106Xv1 --customise PhysicsTools/NanoTuples/nanoTuples_cff.nanoTuples_customizeMC --filein ${infile} --fileout file:${outfile} --customise_commands "process.options.wantSummary = cms.untracked.bool(True)"
#cmsRun Zto4Bntuples/ntuples/python/ntuplizer_cfg.py inputFiles=${infile} outputFile=${outfile} maxEvents=-1 skipEvents=0
cmsRun Zto4Bntuples/ntuples/python/ntuplizer_cfg.py inputFiles=${infile} outputFile=${outfile} maxEvents=-1 skipEvents=0 doFilter=False

xrdcp -f ${outfile} root://cmsdata.phys.cmu.edu//store/user/acrobert/Znano/Apr2024/${evttype}/${outfile}
