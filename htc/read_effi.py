import os, sys, glob
import numpy as np

#key = 'QCD_HT10to15_up20UL18'
key = sys.argv[1]
#logloc = 'friendEPid-Map/outs/friend_{}_skimZJPS1_EPid-Map_file*'.format(key)
logloc = 'nanoPartJTB/outs/nano_PartJTB_skimZJPS1_{}_file*'.format(key)

nalls = []
npasses = []
effis = []
rerun = []

logs = glob.glob(logloc)
for log in logs:

    lines = open(log, 'r').readlines()
    endl = lines[-1].rstrip().split(' ')

    if endl[3] != 'All:' or endl[5] != 'Selected:':
        #print 'Re-run', log, endl
        rerun.append(int(log.split('file')[-1]))
        continue

    nalls.append(int(endl[4]))
    npasses.append(int(endl[6]))
    effis.append(float(endl[8]))
        
nalls = np.array(nalls)
npasses = np.array(npasses)
effis = np.array(effis)

print 'To re-run: ({})'.format(len(rerun)),  rerun
print 'Num alls:', round(nalls.mean(), 1), '+/-', round(nalls.std(), 2)
print 'Num events:', round(npasses.mean(), 1), '+/-', round(npasses.std(), 2)
print 'Efficiency:', round(effis.mean(), 4), '+/-', round(effis.std(), 5)
print 'Overall:', round(float(npasses.sum()) / nalls.sum(), 6)
