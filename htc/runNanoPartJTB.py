import os
import numpy as np
from datadicts import *
#from spawnCondor import spawnCondor
import sys

conditions_upgrade2018 = '106X_upgrade2018_realistic_v15_L1v1'
conditions_mc2017 = '106X_mc2017_realistic_v8'
conditions_F17mc = '94X_mc2017_realistic_v12'

key = sys.argv[1]#'ZHToAATo4B_mc2017UL_m40'#'WZ_mc2017UL'#'ZJets_HT_600to800_mc2017UL' #'ZZTo2Q2L_mc2017UL'#'ZZTo4B01j_mc2017UL'
dataset =  datasets[key]

idxtorun = []
if len(sys.argv) == 4:
    ntorun = int(sys.argv[3])
    start = int(sys.argv[2])
elif len(sys.argv) == 2:
    ntorun = 1000
    start = 0
elif len(sys.argv) > 4:
    #idxtorun = [ int(run[:-1]) for run in sys.argv[2:]] 
    idxtorun = [ int(run) for run in sys.argv[2:]] 
    ntorun = 1000

try:
    dir = t3dirs[key]
except KeyError:
    dir = key
    

#conditions = conditions_F17mc
conditions = conditions_upgrade2018
checkxs = False

def spawnCondor(infile, outfile, evttype, conditions='106X_upgrade2018_realistic_v15_L1v1', submit=False):
    logname = outfile.strip('.root')

    exctbl = 'skimNano.sh' if 'j-g' not in evttype else 'fullNano.sh'

    subcondor_template = """
Executable            = {}
Arguments             = {} {} {}
Should_transfer_files = YES
x509userproxy         = /uscms/home/acrobert/x509up_u57074
Use_x509userproxy     = true
Output                = /uscms/home/acrobert/nobackup/zto4b/CMSSW_10_6_30/src/htc/nanoPartJTB/outs/{}
Error                 = /uscms/home/acrobert/nobackup/zto4b/CMSSW_10_6_30/src/htc/nanoPartJTB/errs/{}
Log                   = /uscms/home/acrobert/nobackup/zto4b/CMSSW_10_6_30/src/htc/nanoPartJTB/logs/{}
Queue
""".format(exctbl, infile, outfile, evttype, logname, logname, logname)

    with open('subcondorNano','a') as sub:
        sub.write(subcondor_template)
        
    if submit:
        os.system('condor_submit subcondorNano')

os.system('. ./mktar.sh > /dev/null 2>&1')
os.system('curl https://raw.githubusercontent.com/cms-sw/genproductions/master/Utilities/calculateXSectionAndFilterEfficiency/genXsec_cfg.py -o ana.py > /dev/null 2>&1')
print('Running NanoAOD w/ ParT Tagger on dataset {} with key {}'.format(dataset, key))
if 'j-g' in key:
    os.system('dasgoclient -query="file dataset={} instance=prod/phys03" > dastmp.txt 2>&1'.format(dataset))
else:
    os.system('dasgoclient -query="file dataset={}" > dastmp.txt 2>&1'.format(dataset))

nfiles = 0
with open('dastmp.txt','r') as dasout:
    daslines = dasout.readlines()
    print('N files: {}'.format(len(daslines)))
    daslines.sort()
    #exit()
    xsarr = []
    errarr = []

    #print('Skipping jobs:'),
    if idxtorun == []:
        lim = min(len(daslines), start+ntorun)# if key != 'TTJets_up20UL18' else len(daslines)
        iterr = range(start, lim)
    else:
        iterr = idxtorun

    for i in iterr:
        thisfile = daslines[i].strip('\n')

        if 'j-g' in dataset:
            fileind = thisfile.split('_')[-1].split('.root')[-2]
        else:
            fileind = i
             
        if key == 'TTJets_up20UL18':
            os.system('dasgoclient -query="site file={}" > sitetmp{}.txt 2>&1'.format(thisfile, fileind))
            check1 = False
            try:
                with open('sitetmp{}.txt'.format(fileind),'r') as siteout:
                    sitelines = siteout.readlines()
                    for line in sitelines:
                        if 'T1_US_FNAL_Disk' in line:
                            check1 = True
                
                os.system('rm sitetmp{}.txt'.format(fileind))
            except IOError:
                pass

            if not check1:
                continue

            check2 = False
            os.system('dasgoclient -query="file={} | grep file.nevents" > evtstmp{}.txt 2>&1'.format(thisfile, fileind))
            try:
                with open('evtstmp{}.txt'.format(fileind),'r') as siteout:
                    sitelines = siteout.readlines()
                    for line in sitelines:
                        try:
                            if int(line.strip('\n')) <= 70000:
                                check2 = True
                        except ValueError:
                            pass
                
                os.system('rm evtstmp{}.txt'.format(fileind))
            except IOError:
                pass
            if not check2:
                continue


        if checkxs:
            os.system('cmsRun ana.py inputFiles="file:root://cms-xrd-global.cern.ch/{}" maxEvents=-1 > xstmp.txt 2>&1'.format(thisfile))

            xs = -1.

            with open('xstmp.txt','r') as xsout:
                xslines = xsout.readlines()
                #print(len(xslines))

                for j in range(len(xslines)):
                    if 'After filter: final cross section' in xslines[j]:
                        eles = xslines[j].strip('\n').split(' ')
                        xsarr.append(float(eles[6]))
                        errarr.append(float(eles[8]))
                        print('xs',i,float(eles[6]))
                        xs = float(eles[6])
                        if len(xsarr) >= 10:
                            checkxs = False
            os.system('rm xstmp.txt')

        if key == 'TTJets_up20UL18':
            print('Adding file {} to queue'.format(fileind))
        if 'j-g' in key:
            addtag = ''
        else: 
            addtag = '_skimZJPS1'
        spawnCondor(thisfile, 'nano_PartJTB{}_{}_file{}.root'.format(addtag, key, fileind), dir, conditions=conditions)
        nfiles += 1
        if nfiles >= ntorun:
                break

    print()

    print('Spawning {} condor jobs...'.format(nfiles))
    os.system('condor_submit subcondorNano')
    print('Submitted!')

    if checkxs:
        xsarr = np.array(xsarr)
        print(xsarr.mean(), xsarr.std())

os.system('rm dastmp.txt && rm subcondorNano')
