#ifndef HLTSkimmer_h
#define HLTSkimmer_h

#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/global/EDFilter.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "DataFormats/METReco/interface/BeamHaloSummary.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/Common/interface/TriggerResults.h"
#include "FWCore/Common/interface/TriggerNames.h"
#include "FWCore/Utilities/interface/RegexMatch.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/BTauReco/interface/JetTag.h"
#include "DataFormats/JetReco/interface/GenJetCollection.h"
#include "DataFormats/JetReco/interface/PFJet.h"

using pat::JetCollection;
using pat::JetRef;

class HLTSkimmer : public edm::global::EDFilter<> {
 public:
  explicit HLTSkimmer(const edm::ParameterSet& iConfig);
  ~HLTSkimmer() override {}

 private:
  virtual void beginJob() override;
  bool filter(edm::StreamID iID, edm::Event& iEvent, const edm::EventSetup& iSetup) const override;
  virtual void endJob() override;
  //edm::EDGetTokenT<reco::GenParticleCollection> genParticleCollectionT_;
  edm::EDGetTokenT<edm::TriggerResults> trgResultsT_;
  edm::EDGetTokenT<JetCollection> jetCollectionT_;

  mutable int nAllEvents = 0;
  mutable int nSelEvents = 0;

};

static const std::vector<std::string> btagnames{"pfDeepCSVJetTags:probb", "pfDeepFlavourJetTags:probb", "pfDeepFlavourJetTags:problepb", "pfCombinedSecondaryVertexV2BJetTags", "pfCombinedInclusiveSecondaryVertexV2BJetTags", "pfCombinedMVAV2BJetTags"};
//static const std::vector<std::string> btagnames{"pfDeepCSVJetTags:probb", "pfDeepFlavourJetTags:probb", "pfDeepFlavourJetTags:problepb"};        
static const std::vector<std::string> bbtagnames{"pfDeepCSVJetTags:probbb", "pfDeepFlavourJetTags:probbb"};


#endif
