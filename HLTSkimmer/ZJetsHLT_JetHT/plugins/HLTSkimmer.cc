#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/global/EDFilter.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "DataFormats/METReco/interface/BeamHaloSummary.h"
#include "DataFormats/Common/interface/TriggerResults.h"

#include "HLTSkimmer/ZJetsHLT_JetHT/interface/HLTSkimmer.h"

const static std::vector<std::string> interestingTriggers = {
  "HLT_AK8PFJet330_TrimMass30_PFAK8BoostedDoubleB_np4_v*",
  "HLT_PFHT1050_v*",
  "HLT_PFJet500_v*",
  "HLT_DoublePFJets116MaxDeta1p6_DoubleCaloBTagDeepCSV_p71_v*",
  "HLT_PFHT800_TrimMass50_v*",
  "HLT_AK8PFJet400_TrimMass30_v*",
  "HLT_AK8PFJet500_v*",
  "HLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59_v*",
  "HLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94_v*",
};

HLTSkimmer::HLTSkimmer(const edm::ParameterSet& iConfig) {
  //genParticleCollectionT_ = consumes<reco::GenParticleCollection>(iConfig.getParameter<edm::InputTag>("genParticleCollection"));
  trgResultsT_ = consumes<edm::TriggerResults>(iConfig.getParameter<edm::InputTag>("trgResults"));
  jetCollectionT_ = consumes<JetCollection>(iConfig.getParameter<edm::InputTag>("jetCollection"));
  produces<bool>();
  
  nAllEvents = 0;
  nSelEvents = 0;

  std::cout << " >> Initialized HLT Skimmer" << std::endl;
}

bool HLTSkimmer::filter(edm::StreamID iID, edm::Event& iEvent, const edm::EventSetup& iSetup) const {

  nAllEvents++;

  edm::Handle<edm::TriggerResults> trgs;
  iEvent.getByToken( trgResultsT_, trgs );

  const edm::TriggerNames &triggerNames = iEvent.triggerNames( *trgs );
  //std::cout << " N triggers:" << trgs->size() << std::endl;
  //for ( unsigned int iT = 0; iT < trgs->size(); iT++ ) {
  //  std::cout << " name["<<iT<<"]: "<<triggerNames.triggerName(iT)<< std::endl;
  //}

  edm::Handle<JetCollection> jets;
  iEvent.getByToken(jetCollectionT_, jets);

  std::vector<edm::Handle<reco::JetTagCollection>> btags;
  for( unsigned int i = 0; i < btagnames.size(); i++ ) {
    edm::Handle<reco::JetTagCollection> tags;
    iEvent.getByLabel(btagnames[i], tags);
    btags.push_back(tags);
  }

  std::vector<edm::Handle<reco::JetTagCollection>> bbtags;
  for( unsigned int i = 0; i < bbtagnames.size(); i++ ) {
    edm::Handle<reco::JetTagCollection> tags;
    iEvent.getByLabel(bbtagnames[i], tags);
    bbtags.push_back(tags);
  }

  int hltAccept = -1;
  //std::string trgName = "HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass3p8_v*";//"HLT_Diphoton30_22_R9Id_OR_IsoCaloId_AND_HE_R9Id_Mass90_v*";
  //std::vector< std::vector<std::string>::const_iterator > trgMatches = edm::regexMatch( triggerNames.triggerNames(), trgName );
  
  std::vector< std::vector<std::string>::const_iterator > trgMatches;

  for (unsigned int i = 0; i < interestingTriggers.size(); i++) {
    std::vector< std::vector<std::string>::const_iterator > theseMatches = edm::regexMatch( triggerNames.triggerNames(), interestingTriggers[i] );
    trgMatches.insert( trgMatches.end(), theseMatches.begin(), theseMatches.end() );
  }
  //std::cout << " N matches: " << trgMatches.size() << std::endl;

  if ( !trgMatches.empty() ) {
    hltAccept = 0;
    for ( auto const& iT : trgMatches ) {
      //std::cout << " name["<<triggerNames.triggerIndex(*iT)<<"]:"<< *iT << " -> " << trgs->accept(triggerNames.triggerIndex(*iT)) << std::endl;
      if ( trgs->accept(triggerNames.triggerIndex(*iT)) ) hltAccept = 1;
      //break;
    }
  }

  int njets_pt20_btagged = 0;
  int njets_pt20_bbtagged = 0;

  for( unsigned int iJ = 0; iJ < jets->size(); iJ++ ) {
    JetRef iJet( jets, iJ );
    double btag = iJet->bDiscriminator(btagnames[1]) + iJet->bDiscriminator(btagnames[2]) + iJet->bDiscriminator(bbtagnames[1]);
    if (iJet->pt() > 20. && btag > 0.2783) njets_pt20_btagged++;
    double bbtag = iJet->bDiscriminator(bbtagnames[1]);
    if (iJet->pt() > 20. && bbtag > 0.2783) njets_pt20_bbtagged++;
  }


  //std::cout << njets_pt30_btagged << std::endl;

  bool passes = hltAccept && ((njets_pt20_btagged >= 2) || (njets_pt20_bbtagged >= 1));

  if (passes) {
    nSelEvents++;
    std::cout << " >> Passes skim!!" << std::endl;
  }

  return passes;
}

void HLTSkimmer::beginJob() {
  // do nothing                                                                                                                                                                                                    
}


void HLTSkimmer::endJob() {
  std::cout << "HLT Skimmer - All: " << nAllEvents << " Selected: " << nSelEvents << " Efficiency: " << ((float) nSelEvents)/((float) nAllEvents) << std::endl;
}

#include "FWCore/Framework/interface/MakerMacros.h"
DEFINE_FWK_MODULE(HLTSkimmer);
