import sys
import FWCore.ParameterSet.Config as cms
import FWCore.ParameterSet.VarParsing as VarParsing

options = VarParsing.VarParsing('analysis')
options.register('skipEvents', 
    default=0, 
    mult=VarParsing.VarParsing.multiplicity.singleton,
    mytype=VarParsing.VarParsing.varType.int,
    info = "skipEvents")
options.register('doFilter',
    default=False,
    mult=VarParsing.VarParsing.multiplicity.singleton,
    mytype=VarParsing.VarParsing.varType.bool,
    info = "do HLT and jet filter")
options.parseArguments()

process = cms.Process("FEVTAnalyzer")

process.load("FWCore.MessageService.MessageLogger_cfi")
process.load("Configuration.StandardSequences.GeometryDB_cff")
process.load("Configuration.StandardSequences.FrontierConditions_GlobalTag_cff")
process.load("FWCore.MessageLogger.MessageLogger_cfi")
process.load('RecoBTag/Configuration/RecoBTag_cff')
process.load('PhysicsTools/PatAlgos/recoLayer0/bTagging_cff')

#process.path(process.btagging)
process.MessageLogger.cerr.FwkReport.reportEvery = 1000
#process.MessageLogger.cerr.FwkReport.reportEvery = 1

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(options.maxEvents)
    )

print " >> Loaded",len(options.inputFiles),"input files from list."

process.source = cms.Source("PoolSource",
    # replace 'myfile.root' with the source file you want to use
    fileNames = cms.untracked.vstring(
      #'file:myfile.root'
      #'file:/eos/uscms/store/user/mba2012/FEVTDEBUG/H125GGgluonfusion_13TeV_TuneCUETP8M1_FEVTDEBUG/*/*/step_full_*.root'
      options.inputFiles
      )
    , skipEvents = cms.untracked.uint32(options.skipEvents)
    #, eventsToProcess = cms.untracked.VEventRange('1:6931:1723687928','1:6932:1723895372')
    #, eventsToProcess = cms.untracked.VEventRange(*evtsToProc)
    #, lumisToProcess = cms.untracked.VLuminosityBlockRange('1:2133-1:2133')
    #, lumisToProcess = cms.untracked.VLuminosityBlockRange('1:3393-1:3393')
    )

#process.GlobalTag = GlobalTag(process.GlobalTag, '106X_upgrade2018_realistic_v15_L1v1', '')

#process.GlobalTag.globaltag = cms.string('106X_mc2017_realistic_v9') # 2017??
process.GlobalTag.globaltag = cms.string('106X_upgrade2018_realistic_v15_L1v1') # 2017??
process.es_prefer_GlobalTag = cms.ESPrefer('PoolDBESSource','GlobalTag')

process.HLTSkimmer = cms.EDFilter('HLTSkimmer'
    , trgResults = cms.InputTag("TriggerResults","","HLT")
    , jetCollection = cms.InputTag('slimmedJets')
    , bTagCollection = cms.InputTag('pfJetProbabilityBJetTags')
)

process.fevt = cms.EDAnalyzer('ntuplizer'
    , photonCollection = cms.InputTag('slimmedPhotons')
    , photonAODCollection = cms.InputTag('photons')                              
#    , conversions = cms.InputTag('reducedEgamma')
    , jetCollection = cms.InputTag('slimmedJets')
    , jetAODCollection = cms.InputTag('ak4PFJetsCHS')
#    , jetPuppiCollection = cms.InputTag('slimmedJetsPuppi')
    , fatJetCollection = cms.InputTag('slimmedJetsAK8')
    , fatJetAODCollection = cms.InputTag('ak8PFJetsCHS')
#    , fatJetPuppiCollection = cms.InputTag('slimmedJetsPuppiAK8')
    , bTagCollection = cms.InputTag('pfJetProbabilityBJetTags')
#    , EBRecHitCollection = cms.InputTag('ecalRecHit:EcalRecHitsEB')
#    , EERecHitCollection = cms.InputTag('ecalRecHit:EcalRecHitsEE')
#    , ESRecHitCollection = cms.InputTag('ecalRecHit:EcalRecHitsES')
    , reducedAODEBRecHitCollection = cms.InputTag('reducedEcalRecHitsEB')
    , reducedAODEERecHitCollection = cms.InputTag('reducedEcalRecHitsEE')
    , reducedAODESRecHitCollection = cms.InputTag('reducedEcalRecHitsES')
    , reducedEBRecHitCollection = cms.InputTag('reducedEgamma:reducedEBRecHits')
    , reducedEERecHitCollection = cms.InputTag('reducedEgamma:reducedEERecHits')
    , reducedESRecHitCollection = cms.InputTag('reducedEgamma:reducedESRecHits')
    , HBHERecHitCollection = cms.InputTag('reducedEgamma:reducedHBHEHits') #works
    , HBHEAODRecHitCollection = cms.InputTag('reducedHcalRecHits:hbhereco') #AOD
    , HBHEHFHORecHitCollection = cms.InputTag('slimmedHcalRecHits:reducedHcalRecHits') # works but empty?
    , genParticleCollection = cms.InputTag('prunedGenParticles')
    , genAODParticleCollection = cms.InputTag('genParticles')
#    , genJetCollection = cms.InputTag('ak4GenJets')
#    , trackCollection = cms.InputTag("isolatedTracks")
    , pfCollection = cms.InputTag("packedPFCandidates")
    , pfAODCollection = cms.InputTag("particleFlow")
    , vertexCollection = cms.InputTag("offlineSlimmedPrimaryVertices")
    , vertexAODCollection = cms.InputTag("offlinePrimaryVertices")
    , z0PVCut  = cms.double(0.1)
#    , rhoLabel = cms.InputTag("fixedGridRhoFastjetAll")
    , trgResults = cms.InputTag("TriggerResults","","HLT")
#    , generator = cms.InputTag("generator")
#    , lhe = cms.InputTag("lhe")
    , pileupCollection     = cms.InputTag("slimmedAddPileupInfo")
    , electronCollection = cms.InputTag('slimmedElectrons')
    , muonCollection = cms.InputTag('slimmedMuons')
    , METCollection = cms.InputTag('slimmedMETs')
    )

process.TFileService = cms.Service("TFileService",
    fileName = cms.string(options.outputFile)
    )

if options.doFilter:
    #process.p = cms.Path(process.fevt*process.HLTSkimmer)
    process.p = cms.Path(process.HLTSkimmer*process.fevt)
else:
    process.p = cms.Path(process.fevt)

process.schedule = cms.Schedule(process.p)
sys.path.append('../../../PhysicsTools')
#import PhysicsTools
#import PhysicsTools.NanoTuples.nanoTuples_cff
#from PhysicsTools.NanoTuples.nanoTuples_cff import nanoTuples_customizeMC
#process = nanoTuples_customizeMC(process)


