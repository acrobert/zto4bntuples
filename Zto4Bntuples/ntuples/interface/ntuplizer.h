#ifndef ntuplizer_h
#define ntuplizer_h

//---------------------------------------------                                                                                                                                                          
//                                                                                                                                                                                                       
// Package: Zto4Bntuples                                                                                                                                                                             
// Class: ntuplizer                                                                                                                                                                                 
//                                                                                                                                                                                                       
// Author: Andrew C. Roberts                                                                                                                                                                             
// Started 2022/9/20                                                                                                                                                                                     
// Last Updated 2022/9/20                                                                                                                                                                                
//                                                                                                                                                                                                       
//---------------------------------------------

#include <vector>
#include "TCanvas.h"

#include "DataFormats/JetReco/interface/PFJet.h"
#include "DataFormats/JetReco/interface/GenJet.h"

#include "DataFormats/BTauReco/interface/JetTag.h"
#include "DataFormats/BTauReco/interface/CandIPTagInfo.h"

// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"

#include "DataFormats/EcalRecHit/interface/EcalRecHitCollections.h"
#include "DataFormats/EcalDigi/interface/EcalDigiCollections.h"
#include "DataFormats/EcalDetId/interface/EcalTrigTowerDetId.h"
#include "Geometry/CaloTopology/interface/EcalBarrelTopology.h"
#include "Geometry/CaloGeometry/interface/CaloGeometry.h"
#include "Geometry/CaloGeometry/interface/CaloCellGeometry.h"
#include "Geometry/Records/interface/CaloGeometryRecord.h"

#include "DataFormats/EgammaCandidates/interface/GsfElectron.h"
#include "DataFormats/EgammaCandidates/interface/Photon.h"
#include "DataFormats/EgammaCandidates/interface/PhotonFwd.h"
#include "DataFormats/PatCandidates/interface/Photon.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/IsolatedTrack.h"
#include "DataFormats/Common/interface/TriggerResults.h"
#include "DataFormats/HcalRecHit/interface/HcalRecHitCollections.h"
#include "Geometry/Records/interface/TrackerDigiGeometryRecord.h"
#include "Geometry/TrackerGeometryBuilder/interface/TrackerGeometry.h"
#include "Geometry/TrackerGeometryBuilder/interface/StripGeomDetUnit.h"
#include "Calibration/IsolatedParticles/interface/DetIdFromEtaPhi.h"
#include "DataFormats/HcalRecHit/interface/HcalRecHitCollections.h"

#include "DQM/HcalCommon/interface/Constants.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/Common/interface/TriggerNames.h"
#include "DataFormats/TrackingRecHit/interface/TrackingRecHit.h"
#include "DataFormats/TrackingRecHit/interface/TrackingRecHitFwd.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/InputTag.h"
#include "FWCore/Utilities/interface/RegexMatch.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TTree.h"
#include "TStyle.h"
#include "TMath.h"
#include "TProfile2D.h"

#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/JetReco/interface/GenJetCollection.h"
#include "DataFormats/Math/interface/deltaR.h"
#include "DataFormats/Math/interface/deltaPhi.h"
#include "Calibration/IsolatedParticles/interface/DetIdFromEtaPhi.h"
#include "RecoEcal/EgammaCoreTools/interface/EcalClusterLazyTools.h"
#include "DataFormats/ParticleFlowCandidate/interface/PFCandidate.h"
#include "DataFormats/PatCandidates/interface/PackedCandidate.h"

#include "SimDataFormats/PileupSummaryInfo/interface/PileupSummaryInfo.h"
#include "SimDataFormats/GeneratorProducts/interface/GenEventInfoProduct.h"
#include "SimDataFormats/GeneratorProducts/interface/LHEEventProduct.h"

using pat::ElectronCollection;
using pat::ElectronRef;
using pat::MuonCollection;
using pat::MuonRef;
//using pat::TauCollection;
//using pat::TauRef;
using pat::JetCollection;
using pat::JetRef;
using pat::PhotonCollection;
using pat::PhotonRef;
using pat::METCollection;
using pat::METRef;


class ntuplizer : public edm::one::EDAnalyzer<edm::one::SharedResources>  {
  public:
    explicit ntuplizer(const edm::ParameterSet&);
    ~ntuplizer();

    static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);


  private:
    virtual void beginJob() override;
    virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
    virtual void endJob() override;

    // ----------member data ---------------------------
    edm::EDGetTokenT<ElectronCollection> electronCollectionT_;
    edm::EDGetTokenT<MuonCollection> muonCollectionT_;
    edm::EDGetTokenT<PhotonCollection> photonCollectionT_;
    edm::EDGetTokenT<JetCollection> jetCollectionT_;
    edm::EDGetTokenT<JetCollection> jetPuppiCollectionT_;
    edm::EDGetTokenT<JetCollection> fatJetCollectionT_;
    edm::EDGetTokenT<JetCollection> fatJetPuppiCollectionT_;    
    edm::EDGetTokenT<METCollection> METCollectionT_;
    edm::EDGetTokenT<EcalRecHitCollection> EBRecHitCollectionT_;
    edm::EDGetTokenT<EcalRecHitCollection> EERecHitCollectionT_;
    edm::EDGetTokenT<EcalRecHitCollection> ESRecHitCollectionT_;
    edm::EDGetTokenT<HFRecHitCollection> HFRecHitCollectionT_;
    edm::EDGetTokenT<HORecHitCollection> HORecHitCollectionT_;
    edm::EDGetTokenT<HBHERecHitCollection> HBHERecHitCollectionT_;
    edm::EDGetTokenT<HBHERecHitCollection> HBHEAODRecHitCollectionT_;
    edm::EDGetTokenT<EcalRecHitCollection> AODEBRecHitCollectionT_;
    edm::EDGetTokenT<EcalRecHitCollection> AODEERecHitCollectionT_;
    edm::EDGetTokenT<EcalRecHitCollection> AODESRecHitCollectionT_;
    edm::EDGetTokenT<EcalRecHitCollection> RECOEBRecHitCollectionT_;
    edm::EDGetTokenT<EcalRecHitCollection> RECOEERecHitCollectionT_;
    edm::EDGetTokenT<EcalRecHitCollection> RECOESRecHitCollectionT_;
    edm::EDGetTokenT<reco::GenParticleCollection> genParticleCollectionT_;
    edm::EDGetTokenT<reco::GenJetCollection> genJetCollectionT_;
    edm::EDGetTokenT<pat::IsolatedTrackCollection> trackCollectionT_;
    edm::EDGetTokenT<double> rhoLabel_;
    edm::EDGetTokenT<edm::TriggerResults> trgResultsT_;
    edm::EDGetTokenT<GenEventInfoProduct> genInfoT_;
    edm::EDGetTokenT<LHEEventProduct> lheEventT_;
    

    edm::EDGetTokenT<reco::VertexCollection> vertexCollectionT_;
    typedef std::vector<pat::PackedCandidate>  PFCollection;
    edm::EDGetTokenT<PFCollection> pfCollectionT_;
    edm::EDGetTokenT<std::vector<PileupSummaryInfo> > puCollection_;

    TTree* EvtTree;
    
    // event IDs
    unsigned long long eventId;
    unsigned int runId;
    unsigned int lumiId;

    // objects for temp storage 
    unsigned int nZ = 0;
    int n_decays_e = 0;
    int n_decays_mu = 0;
    int n_decays_tau = 0;
    int n_pass_1L = 0;
    //int n_pass_bs = 0;
    reco::GenParticleRef Z1;
    reco::GenParticleRef Z2;
    int nZ1b;
    int nZ2b;
    std::vector<const reco::Candidate*> Z1bs;
    std::vector<const reco::Candidate*> Z2bs;
    int n2bfj = 0;
    int n3bfj = 0;
    int n4bfj = 0;
    int nhasjets = 0;
    bool save_this_event = false;
    std::vector<float> img_eta;
    std::vector<float> img_phi;
    std::vector<int> img_type; // duo - 2; trio - 3; quad - 4 
    int n4resB = 0;
    std::vector<float> metvec;

    
    //void branchesDiPhotonSel ( TTree*, edm::Service<TFileService>& );
    //bool runDiPhotonSel ( const edm::Event&, const edm::EventSetup& );
    //void fillDiPhotonSel ( const edm::Event&, const edm::EventSetup& );

    // helper functions
    void printProgeny ( const reco::GenParticleRef );
    int countBDaughters( const reco::GenParticleRef );
    void listBDaughters( const reco::GenParticleRef, std::vector<const reco::Candidate*>& );
    void listQDaughters( const reco::GenParticleRef, std::vector<const reco::Candidate*>&, unsigned int );
    void listEndQs( const reco::GenParticleRef, std::vector<const reco::Candidate*>& );
    std::vector<int> ieta_iphi_from_eta_phi( const edm::EventSetup&, float, float );
    bool cand_in_vector( const reco::Candidate*, std::vector<const reco::Candidate*> );
    void printJetTags( JetRef );
    std::vector<double> fillJetTags( JetRef );
    bool hasZ4B( const edm::Event& );

    // find event with Z->4B
    bool runZ4BSel ( const edm::Event&, const edm::EventSetup& );
    // run single lepton selection
    bool run1LSel (const edm::Event&, const edm::EventSetup& );
    // run single photon selection
    bool runGammaSel (const edm::Event&, const edm::EventSetup& );
    // run jet-only selection
    bool runJetSel (const edm::Event&, const edm::EventSetup& );

    // Object vectors
    void branchesObjVectors ( TTree*, edm::Service<TFileService>& );
    void fillObjVectors ( const edm::Event&, const edm::EventSetup& ); 

    // Generator particles
    void branchesGenParts ( TTree*, edm::Service<TFileService>& );
    void fillGenParts ( const edm::Event&, const edm::EventSetup& ); 

    // B quark duos and trios
    void branchesBGroups ( TTree*, edm::Service<TFileService>& );
    void fillBGroups ( const edm::Event&, const edm::EventSetup& );

    // ECal images
    void branchesECalImages ( TTree*, edm::Service<TFileService>& );
    void fillECalImages ( const edm::Event&, const edm::EventSetup& );

    // HCal images
    void branchesHCalImages ( TTree*, edm::Service<TFileService>& );
    void fillHCalImages ( const edm::Event&, const edm::EventSetup& );

    // PFT images
    void branchesPFTImages ( TTree*, edm::Service<TFileService>& );
    void fillPFTImages ( const edm::Event&, const edm::EventSetup& );

    // PFTracks
    void branchesPFTracks ( TTree*, edm::Service<TFileService>& );
    void fillPFTracks ( const edm::Event&, const edm::EventSetup& );
    void branchesPFCands ( TTree*, edm::Service<TFileService>& );
    void fillPFCands ( const edm::Event&, const edm::EventSetup& );

    // RecHits
    void branchesRecHits ( TTree*, edm::Service<TFileService>& );
    void fillRecHits ( const edm::Event&, const edm::EventSetup& );

    // Jets
    void branchesJets ( TTree*, edm::Service<TFileService>& );
    void fillJets ( const edm::Event&, const edm::EventSetup& );
    void branchesJetTags ( TTree*, edm::Service<TFileService>& );
    void fillJetTags ( const edm::Event&, const edm::EventSetup& );

};

//
// constants, enums and typedefs
//
static const float zs = 0.;
static const int nEE = 2;

//static const int crop_size = 32;
static const int crop_size = 160;
static const int crop_shift = crop_size/2 - 1;
static const int hcrop_size = 32;
static const int hcrop_shift = hcrop_size/2 - 1;
//static const bool debug = true;
static const bool debug = false;
static const bool AOD = false;
//static const bool EPid = true;
static const bool EPid = false;
//static const bool map = true;
static const bool map = false;
static const bool EPidMap = true;
//static const bool EPidMap = false;
static const bool background = false;
static const bool isdata = true;

static const int EB_IPHI_MIN = EBDetId::MIN_IPHI;//1;
static const int EB_IPHI_MAX = EBDetId::MAX_IPHI;//360;
static const int EB_IETA_MIN = EBDetId::MIN_IETA;//1;
static const int EB_IETA_MAX = EBDetId::MAX_IETA;//85;
static const int EE_MIN_IX = EEDetId::IX_MIN;//1;
static const int EE_MIN_IY = EEDetId::IY_MIN;//1;
static const int EE_MAX_IX = EEDetId::IX_MAX;//100;
static const int EE_MAX_IY = EEDetId::IY_MAX;//100;
static const int EE_NC_PER_ZSIDE = EEDetId::IX_MAX*EEDetId::IY_MAX; // 100*100

static const int HBHE_IETA_MAX_FINE = 20;
static const int HBHE_IETA_MAX_HB = hcaldqm::constants::IETA_MAX_HB;//16;                                                                                                                                
static const int HBHE_IETA_MIN_HB = hcaldqm::constants::IETA_MIN_HB;//1                                                                                                                                  
static const int HBHE_IETA_MAX_HE = hcaldqm::constants::IETA_MAX_HE;//29;                                                                                                                                
static const int HBHE_IETA_MAX_EB = hcaldqm::constants::IETA_MAX_HB + 1; // 17                                                                                                                           
static const int HBHE_IPHI_NUM = hcaldqm::constants::IPHI_NUM;//72;                                                                                                                                      
static const int HBHE_IPHI_MIN = hcaldqm::constants::IPHI_MIN;//1;                                                                                                                                       
static const int HBHE_IPHI_MAX = hcaldqm::constants::IPHI_MAX;//72;                                                                                                                                       
static const int ECAL_IETA_MAX_EXT = 140;

//new ones
static float lepptthresh = 15.;
static float phoptthresh = 15.;
// met cut? higher or lower limit?                                                                                                                                                                       
//static float bptthresh = 20.;
//static float bbptthresh = 20.;
// three scenarios: 4x b, 2x b + 1x bb, 1x b + 1x bb                                                                                                                                                      
static float btagthresh = 0.0490;
static float bbtagthresh = 0.0490;


//
// static data member definitions
//

static const std::vector<std::string> btagnames{"pfDeepCSVJetTags:probb", "pfDeepFlavourJetTags:probb", "pfDeepFlavourJetTags:problepb", "pfCombinedSecondaryVertexV2BJetTags", "pfCombinedInclusiveSecondaryVertexV2BJetTags", "pfCombinedMVAV2BJetTags"};
//static const std::vector<std::string> btagnames{"pfDeepCSVJetTags:probb", "pfDeepFlavourJetTags:probb", "pfDeepFlavourJetTags:problepb"};
static const std::vector<std::string> bbtagnames{"pfDeepCSVJetTags:probbb", "pfDeepFlavourJetTags:probbb"};



#endif
