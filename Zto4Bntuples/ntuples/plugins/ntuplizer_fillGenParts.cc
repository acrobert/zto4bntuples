// --------------------------------------------
//
// Package:    Zto4Bntuples
// Class:      ntuplizer
// 
//
// Author: Andrew C. Roberts
// Started 2022/9/20
// Last Updated 2022/10/4
//
// --------------------------------------------

#include "Zto4Bntuples/ntuples/interface/ntuplizer.h"
#include <stdlib.h>

int nGenPart;
std::vector<int> GenPart_pdgId;
std::vector<int> GenPart_status;
std::vector<int> GenPart_statusFlags;
std::vector<int> GenPart_genPartIdxMother;
std::vector<double> GenPart_eta;
std::vector<double> GenPart_phi;
std::vector<double> GenPart_mass;
std::vector<double> GenPart_pt;
std::vector<int> GenTree_nFinalBs;

// initialize branches
void ntuplizer::branchesGenParts ( TTree* tree, edm::Service<TFileService> &fs )
{

  EvtTree->Branch("nGenPart",                 &nGenPart);
  EvtTree->Branch("GenPart_pdgId",            &GenPart_pdgId);
  EvtTree->Branch("GenPart_status",           &GenPart_status);
  EvtTree->Branch("GenPart_statusFlags",      &GenPart_statusFlags);
  EvtTree->Branch("GenPart_genPartIdxMother", &GenPart_genPartIdxMother);
  EvtTree->Branch("GenPart_eta",              &GenPart_eta);
  EvtTree->Branch("GenPart_phi",              &GenPart_phi);
  EvtTree->Branch("GenPart_mass",             &GenPart_mass);
  EvtTree->Branch("GenPart_pt",               &GenPart_pt);
  EvtTree->Branch("GenTree_nFinalBs",         &GenTree_nFinalBs);

}

// fill object 4-vector histograms
void ntuplizer::fillGenParts(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{

  nGenPart = 0;
  GenPart_pdgId.clear();
  GenPart_status.clear();
  GenPart_statusFlags.clear();
  GenPart_genPartIdxMother.clear();
  GenPart_eta.clear();
  GenPart_phi.clear();
  GenPart_mass.clear();
  GenPart_pt.clear();
  GenTree_nFinalBs.clear();

  edm::Handle<reco::GenParticleCollection> genParticles;
  iEvent.getByToken(genParticleCollectionT_, genParticles);

  for ( unsigned int iG = 0; iG < genParticles->size(); iG++ ) {
    reco::GenParticleRef iGen( genParticles, iG );
    nGenPart++;

    if( iGen->pdgId() == 23 && iGen->status() == 62 ) {
      //nb = countBDaughters(iGen);
      int nb = countBDaughters(iGen);
      if (nb > 0) std::cout << "Evt w/ " << nb << " bs; flags "<< iGen->statusFlags().flags_.to_ulong() << std::endl;
      //printProgeny(iGen);
      GenTree_nFinalBs.push_back(nb);
    }

    GenPart_pdgId.push_back(iGen->pdgId());
    GenPart_status.push_back(iGen->status());
    GenPart_eta.push_back(iGen->eta());
    GenPart_phi.push_back(iGen->phi());
    GenPart_pt.push_back(iGen->pt());
    GenPart_mass.push_back(iGen->mass());
    GenPart_statusFlags.push_back(iGen->statusFlags().flags_.to_ulong());

    int motherIdx = -1;
    if (iGen->numberOfMothers() == 0) {
      GenPart_genPartIdxMother.push_back(motherIdx);
      continue;
    }
    const reco::Candidate* mother = iGen->mother(0);

    for ( unsigned int iH = 0; iH < genParticles->size(); iH++ ) {
      if (iG == iH) continue;
      reco::GenParticleRef iHen( genParticles, iH );

      if (iHen->pt() == mother->pt() && iHen->pdgId() == mother->pdgId() && iHen->status() == mother->status()) {
	bool matchesDaughter = false;
	for ( unsigned int iJ = 0; iJ < iHen->numberOfDaughters(); iJ++) {
	  const reco::Candidate* iJen = iHen->daughter(iJ);
	  if (iJen->pt() == iGen->pt() && iJen->pdgId() == iGen->pdgId() && iJen->status() == iGen->status()) matchesDaughter = true;
	}
	
	//GenPart_genPartIdxMother.push_back(iH);
	if (matchesDaughter) motherIdx = iH;
	break;
      }
    }
    GenPart_genPartIdxMother.push_back(motherIdx);
  }
}

