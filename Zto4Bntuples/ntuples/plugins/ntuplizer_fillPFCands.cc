// -------------------------------------------- 
//                                              
// Package:    Zto4Bntuples                     
// Class:      ntuplizer                        
//                                              
//                                              
// Author: Andrew C. Roberts                    
// Started 2023/5/12                            
// Last Updated 2023/5/12                       
//                                              
// -------------------------------------------- 

#include "Zto4Bntuples/ntuples/interface/ntuplizer.h"
#include <stdlib.h>

int map_nPFCands;
std::vector<double> map_PFCands_pT;
std::vector<double> map_PFCands_phi;
std::vector<double> map_PFCands_eta;
std::vector<double> map_PFCands_d0;
std::vector<double> map_PFCands_z0;
std::vector<double> map_PFCands_Q;
std::vector<double> map_PFCands_E;
std::vector<double> map_PFCands_m;
std::vector<int> map_PFCands_nPixHits;
std::vector<int> map_PFCands_nHits;
std::vector<int> map_PFCands_status;
std::vector<double> map_PFCands_rawCaloFrac;
std::vector<double> map_PFCands_rawHcalFrac;
std::vector<double> map_PFCands_caloFrac;
std::vector<double> map_PFCands_hcalFrac;
std::vector<double> map_PFCands_time;
std::vector<int> map_PFCands_pdgId;
std::vector<std::vector<double>> map_PFCands_mva;

void ntuplizer::branchesPFCands ( TTree* tree, edm::Service<TFileService> &fs )
{

  EvtTree->Branch("map_nPFCands", &map_nPFCands);
  EvtTree->Branch("map_PFCands_pT", &map_PFCands_pT);
  EvtTree->Branch("map_PFCands_phi", &map_PFCands_phi);
  EvtTree->Branch("map_PFCands_eta", &map_PFCands_eta);
  EvtTree->Branch("map_PFCands_d0", &map_PFCands_d0);
  EvtTree->Branch("map_PFCands_z0", &map_PFCands_z0);
  EvtTree->Branch("map_PFCands_Q", &map_PFCands_Q);
  EvtTree->Branch("map_PFCands_E", &map_PFCands_E);
  EvtTree->Branch("map_PFCands_m", &map_PFCands_m);
  EvtTree->Branch("map_PFCands_nPixHits", &map_PFCands_nPixHits);
  EvtTree->Branch("map_PFCands_nHits", &map_PFCands_nHits);
  EvtTree->Branch("map_PFCands_status", &map_PFCands_status);
  EvtTree->Branch("map_PFCands_rawCaloFrac", &map_PFCands_rawCaloFrac);
  EvtTree->Branch("map_PFCands_rawHcalFrac", &map_PFCands_rawHcalFrac);
  EvtTree->Branch("map_PFCands_caloFrac", &map_PFCands_caloFrac);
  EvtTree->Branch("map_PFCands_hcalFrac", &map_PFCands_hcalFrac);
  EvtTree->Branch("map_PFCands_time", &map_PFCands_time);
  EvtTree->Branch("map_PFCands_pdgId", &map_PFCands_pdgId);
  EvtTree->Branch("map_PFCands_mva", &map_PFCands_mva);

}

void ntuplizer::fillPFCands ( const edm::Event& iEvent, const edm::EventSetup& iSetup ) {

  edm::Handle<PFCollection> pfCandsH_;
  iEvent.getByToken( pfCollectionT_, pfCandsH_ );

  // Provides access to global cell position                                                                                                                                                          
  edm::ESHandle<CaloGeometry> caloGeomH_;
  iSetup.get<CaloGeometryRecord>().get( caloGeomH_ );
  const CaloGeometry* caloGeom = caloGeomH_.product();

  edm::Handle<reco::VertexCollection> vertexInfo;
  iEvent.getByToken(vertexCollectionT_, vertexInfo);
  const reco::VertexCollection& vtxs = *vertexInfo;

  map_PFCands_pT.clear();
  map_PFCands_phi.clear();
  map_PFCands_eta.clear();
  map_PFCands_d0.clear();
  map_PFCands_z0.clear();
  map_PFCands_Q.clear();
  map_PFCands_E.clear();
  map_PFCands_m.clear();
  map_PFCands_nPixHits.clear();
  map_PFCands_nHits.clear();
  map_PFCands_status.clear();
  map_PFCands_rawCaloFrac.clear();
  map_PFCands_rawHcalFrac.clear();
  map_PFCands_caloFrac.clear();
  map_PFCands_hcalFrac.clear();
  map_PFCands_time.clear();
  map_PFCands_pdgId.clear();
  map_PFCands_mva.clear();

  //if (!hasZ4B(iEvent)) return;
  
  //double eta, phi, d0, z0;
  int n = 0;

  for ( PFCollection::const_iterator iPFC = pfCandsH_->begin();
	iPFC != pfCandsH_->end(); ++iPFC ) {

    //const reco::Track* thisTrk = iPFC->bestTrack();
    //if(!thisTrk) continue;

    //const reco::Track thisTrack = *thisTrk;
    //double thisTrkPt = thisTrk->pt();
    //if (thisTrkPt <= zs) continue;

    //double thisTrkQPt = (thisTrk->pt()*thisTrk->charge());
    //double thisTrkQ = (double) thisTrk->charge();    

    //eta = iPFC->eta();
    //phi = iPFC->phi();
    //if ( std::abs(eta) > 2.5 ) continue;

    //d0 =  ( !vtxs.empty() ? thisTrk->dxy(vtxs[0].position()) : thisTrk->dxy() );
    //z0 =  ( !vtxs.empty() ? thisTrk->dz(vtxs[0].position())  : thisTrk->dz() );

    //map_PFCands_pT.push_back(thisTrkPt);
    //map_PFCands_phi.push_back(phi);
    //map_PFCands_eta.push_back(eta);
    //map_PFCands_d0.push_back(d0);
    //map_PFCands_z0.push_back(z0);
    //map_PFCands_Q.push_back(thisTrkQ);
    
    map_PFCands_pT.push_back(iPFC->pt());
    map_PFCands_phi.push_back(iPFC->phi());
    map_PFCands_eta.push_back(iPFC->eta());
    map_PFCands_d0.push_back(iPFC->dxy());
    map_PFCands_z0.push_back(iPFC->dz());
    map_PFCands_Q.push_back(iPFC->charge());

    map_PFCands_E.push_back(iPFC->energy());
    map_PFCands_m.push_back(iPFC->mass());
    map_PFCands_nPixHits.push_back(iPFC->numberOfPixelHits());
    map_PFCands_nHits.push_back(iPFC->numberOfHits());
    map_PFCands_status.push_back(iPFC->status());
    map_PFCands_rawCaloFrac.push_back(iPFC->rawCaloFraction());
    map_PFCands_rawHcalFrac.push_back(iPFC->rawHcalFraction());
    map_PFCands_caloFrac.push_back(iPFC->caloFraction());
    map_PFCands_hcalFrac.push_back(iPFC->hcalFraction());
    map_PFCands_time.push_back(iPFC->time());
    map_PFCands_pdgId.push_back(iPFC->pdgId());
    n++;
    
  }
  map_nPFCands = n;
  //std::cout << n <<std::endl;
}
