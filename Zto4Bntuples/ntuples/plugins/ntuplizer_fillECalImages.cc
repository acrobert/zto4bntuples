// --------------------------------------------
//
// Package:    Zto4Bntuples
// Class:      ntuplizer
// 
//
// Author: Andrew C. Roberts
// Started 2022/10/4
// Last Updated 2022/10/4
//
// --------------------------------------------

#include "Zto4Bntuples/ntuples/interface/ntuplizer.h"
#include <stdlib.h>

std::vector<std::vector<float>> duo_ecal_img;
std::vector<std::vector<float>> trio_ecal_img;
std::vector<std::vector<float>> quad_ecal_img;

// initialize branches
void ntuplizer::branchesECalImages ( TTree* tree, edm::Service<TFileService> &fs )
{

  EvtTree->Branch("duo_ecal_img", &duo_ecal_img);
  EvtTree->Branch("trio_ecal_img", &trio_ecal_img);
  EvtTree->Branch("quad_ecal_img", &quad_ecal_img);

}

// fill b group object histograms
void ntuplizer::fillECalImages(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{

  std::vector<float> ecal_img;

  edm::Handle<EcalRecHitCollection> EBRecHitsH;
  iEvent.getByToken(EBRecHitCollectionT_, EBRecHitsH);
  
  edm::ESHandle<CaloGeometry> caloGeomH;
  iSetup.get<CaloGeometryRecord>().get(caloGeomH);
  const CaloGeometry* caloGeom = caloGeomH.product();

  duo_ecal_img.clear();  
  trio_ecal_img.clear();  
  quad_ecal_img.clear();

  int ieta, iphi, ieta_crop, iphi_crop, ieta_shift, iphi_shift, idx;
  if( save_this_event ) {
    for( unsigned int iG = 0; iG < img_type.size(); iG++ ) {
      float ctr_eta = img_eta[iG];
      float ctr_phi = img_phi[iG];
      
      ecal_img.assign( crop_size*crop_size, 0. );
      DetId ctrId( spr::findDetIdECAL( caloGeom, ctr_eta, ctr_phi, false ) );
      if ( ctrId.subdetId() != EcalBarrel ) continue;
      EBDetId ctrEBId( ctrId );

      ieta_shift = ctrEBId.ieta() - hcrop_shift;
      iphi_shift = ctrEBId.iphi() - hcrop_shift;

      for(EcalRecHitCollection::const_iterator iRHit = EBRecHitsH->begin();
	  iRHit != EBRecHitsH->end();
	  ++iRHit) {

	if ( iRHit->energy() < zs ) continue;
	EBDetId ebId( iRHit->id() );
	ieta = ebId.ieta() > 0 ? ebId.ieta()-1 : ebId.ieta(); // [-85,...,-1,0,...,84]
	iphi = ebId.iphi()-1; // [0,...,359]
	ieta += EBDetId::MAX_IETA; // [0,...,169]
	
	ieta_crop = ieta - ieta_shift;
	iphi_crop = iphi - iphi_shift;
	if ( iphi_crop >= EBDetId::MAX_IPHI ) iphi_crop = iphi_crop - EBDetId::MAX_IPHI; // get wrap-around hits                                                                                         
	if ( iphi_crop < 0 ) iphi_crop = iphi_crop + EBDetId::MAX_IPHI; // get wrap-around hits                                                                                                          
	
	// select image
	if ( ieta_crop < 0 || ieta_crop > crop_size-1 ) continue;
	if ( iphi_crop < 0 || iphi_crop > crop_size-1 ) continue;

	idx = ieta_crop*crop_size + iphi_crop;
	ecal_img[idx] = iRHit->energy();

      }

      if( img_type[iG] == 2 ) duo_ecal_img.push_back(ecal_img);
      if( img_type[iG] == 3 ) trio_ecal_img.push_back(ecal_img);
      if( img_type[iG] == 4 ) quad_ecal_img.push_back(ecal_img);

    }
  }
}

