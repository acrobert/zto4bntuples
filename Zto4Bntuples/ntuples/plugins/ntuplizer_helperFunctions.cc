// --------------------------------------------
//
// Package:    Zto4Bntuples
// Class:      ntuplizer
// 
//
// Author: Andrew C. Roberts
// Started 2022/9/30
// Last Updated 2022/10/1
//
// --------------------------------------------

#include "Zto4Bntuples/ntuples/interface/ntuplizer.h"
#include <stdlib.h>
#include <math.h>

// recursively print all daughter particles                                                                                                                                                              
void ntuplizer::printProgeny ( const reco::GenParticleRef part ) {
  void printProgeny_rec(const reco::Candidate* part, unsigned int l);
  std::cout << "L0 - ID: " << part->pdgId() << " status: " << part->status() << " pT: " << part->pt() << std::endl;

  for (unsigned int i = 0; i <part->numberOfDaughters(); i++) {
    printProgeny_rec(part->daughter(i), 0);
  }
  std::cout << std::endl;
}

// function for recursion above (type needs to be Candidate)                                                                                                                                             
void printProgeny_rec ( const reco::Candidate* part, unsigned int l ) {
  if ( abs(part->pdgId()) == 5 || abs(part->pdgId()) == 21 || abs(part->pdgId()) == 22 || abs(part->pdgId()) == 23 || abs(part->pdgId()) == 24 ) {
    std::cout << std::endl << "L" << l+1 << " - ID: " << part->pdgId() << " status: " << part->status() << " pT: " << part->pt() << " eta: " << part->eta() << " phi: " << part->phi();
  } else { 
    std::cout << " L" << l+1;
  }

  for ( unsigned int i = 0; i < part->numberOfDaughters(); i++) {
    if( abs(part->pdgId()) <= 40 ) {
      printProgeny_rec(part->daughter(i),l+1);
    } else {
      printProgeny_rec(part->daughter(i),l+1);
    }
  }
}

// count number of b daughters that decay to not-b's
int ntuplizer::countBDaughters( const reco::GenParticleRef part ) {
  int countBDaughters_rec(const reco::Candidate* part2, std::vector<const reco::Candidate*>& blist2 );
  std::vector<const reco::Candidate*> blist;

  int nb = 0;
  if (debug) std::cout << "countbdaughters ndaughters " << part->numberOfDaughters() << std::endl;
  for (unsigned int i = 0; i < part->numberOfDaughters(); i++) {
    //std::cout << "daughter " << part->daughter(i)->pdgId() << std::endl;
    if (debug) std::cout << "daughter " << part->daughter(i)->pdgId() << std::endl;
    nb += countBDaughters_rec( part->daughter(i), blist );
  }

  return nb;
}

// function for recursion above (type needs to be Candidate)
int countBDaughters_rec( const reco::Candidate* part, std::vector<const reco::Candidate*>& blist  ) {

  bool hasbdaughter = false;
  int nb = 0;
  for ( unsigned int i = 0; i < part->numberOfDaughters(); i++) {
    nb += countBDaughters_rec( part->daughter(i), blist );
    
    if( abs(part->daughter(i)->pdgId()) == 5 ) {
      hasbdaughter = true;
    }
  }

  if( abs(part->pdgId()) == 5 && !hasbdaughter ) {
    bool bexists = false;
    for( const reco::Candidate* p : blist ) {
      if( p->pt() == part->pt() && p->eta() == part->eta() && p->phi() == part->phi() ) bexists = true;
    }

    if( !bexists ) {
      //std::cout << "found end state b with status " << part->status() << std::endl;
      blist.push_back(part);
      nb += 1;
    }
  }

  return nb;
}

// fill supplied vector with b daughter objects that decay to not-b's
//void ntuplizer::listBDaughters( const reco::GenParticleRef part, std::vector<const reco::Candidate*>& blist ) {
//  void listBDaughters_rec(const reco::Candidate* part, std::vector<const reco::Candidate*>& blist );
//
//  for (unsigned int i = 0; i < part->numberOfDaughters(); i++) {
//    listBDaughters_rec(part->daughter(i), blist);
//  }
//}
//
//// function for recursion above (type needs to be Candidate)
//void listBDaughters_rec( const reco::Candidate* part, std::vector<const reco::Candidate*>& blist ) {
//
//  bool hasbdaughter = false;
//
//  for ( unsigned int i = 0; i < part->numberOfDaughters(); i++) {
//    listBDaughters_rec( part->daughter(i), blist );
//
//    if( abs(part->daughter(i)->pdgId()) == 5 ) {
//      hasbdaughter = true;
//    }
//  }
//
//  if( abs(part->pdgId()) == 5 && !hasbdaughter ) {
//    bool bexists = false;
//    for( const reco::Candidate* p : blist ) {
//      if( p->pt() == part->pt() && p->eta() == part->eta() && p->phi() == part->phi() ) bexists = true;
//    }
//
//    if( !bexists ) {
//      blist.push_back(part);
//    }
//  }
//}

void ntuplizer::listBDaughters( const reco::GenParticleRef part, std::vector<const reco::Candidate*>& blist ) {
  if (debug) std::cout << "ListBDaughters " << part->pdgId() << " " << part->status() << std::endl;
  listQDaughters(part, blist, 5);
}

// fill supplied vector with any given final quark daughter objects that decay to something else
void ntuplizer::listQDaughters( const reco::GenParticleRef part, std::vector<const reco::Candidate*>& qlist, unsigned int qkId ) {
  void listQDaughters_rec(const reco::Candidate* part, std::vector<const reco::Candidate*>& qlist, unsigned int qkId);

  for (unsigned int i = 0; i < part->numberOfDaughters(); i++) {
    listQDaughters_rec(part->daughter(i), qlist, qkId);
  }
}

// function for recursion above (type needs to be Candidate)
void listQDaughters_rec( const reco::Candidate* part, std::vector<const reco::Candidate*>& qlist, unsigned int qkId ) {

  bool hasqdaughter = false;

  for ( unsigned int i = 0; i < part->numberOfDaughters(); i++) {
    listQDaughters_rec( part->daughter(i), qlist, qkId );

    if( abs(part->daughter(i)->pdgId()) == qkId ) {
      hasqdaughter = true;
    }
  }

  if( abs(part->pdgId()) == qkId && !hasqdaughter ) {
    bool qexists = false;
    for( const reco::Candidate* p : qlist ) {
      if( p->pt() == part->pt() && p->eta() == part->eta() && p->phi() == part->phi() ) qexists = true;
    }

    if( !qexists ) {
      qlist.push_back(part);
    }
  }
}

// fill supplied vector with any given final quark daughter objects that decay to something else
void ntuplizer::listEndQs( const reco::GenParticleRef part, std::vector<const reco::Candidate*>& qlist) {
  void listEndQs_rec(const reco::Candidate* part, std::vector<const reco::Candidate*>& qlist);

  for (unsigned int i = 0; i < part->numberOfDaughters(); i++) {
    listEndQs_rec(part->daughter(i), qlist);
  }
}

// function for recursion above (type needs to be Candidate)
void listEndQs_rec( const reco::Candidate* part, std::vector<const reco::Candidate*>& qlist) {

  bool hasqdaughter = false;

  for ( unsigned int i = 0; i < part->numberOfDaughters(); i++) {
    listEndQs_rec( part->daughter(i), qlist );

    if( abs(part->daughter(i)->pdgId()) >= 1 && abs(part->daughter(i)->pdgId()) <= 6 ) {
      hasqdaughter = true;
    }
  }
    
  if( abs(part->pdgId()) >= 1 && abs(part->pdgId()) <= 6 && !hasqdaughter ) {
    bool qexists = false;
    for( const reco::Candidate* p : qlist ) {
      if( p->pt() == part->pt() && p->eta() == part->eta() && p->phi() == part->phi() ) qexists = true;
    }

    if( !qexists ) {
      qlist.push_back(part);
    }
  }
}

std::vector<int> ntuplizer::ieta_iphi_from_eta_phi( const edm::EventSetup& iSetup, float eta, float phi ) {

  const float ecalrho = 1.203; //in meters, approximately
  float theta = 2*atan( exp(-eta) );
  float z = ecalrho/tan(theta);
  float rad = sqrt( ecalrho*ecalrho + z*z );

  const GlobalPoint r (theta, phi, rad);

  edm::ESHandle<CaloGeometry> caloGeomH;
  iSetup.get<CaloGeometryRecord>().get(caloGeomH);
  const CaloGeometry* caloGeom = caloGeomH.product();
  const CaloSubdetectorGeometry* ecalGeom = caloGeom->getSubdetectorGeometry( DetId::Ecal, 1 );

  DetId detid = ecalGeom->getClosestCell( r );
  EBDetId ebdetid ( detid );

  std::vector<int> ret = { ebdetid.ieta(), ebdetid.iphi() };
  return ret;
  
  //for(EcalRecHitCollection::const_iterator iRHit = EBRecHitsH->begin();
  //    iRHit != EBRecHitsH->end();
  //    ++iRHit) {
  //
  //  if ( iRHit->energy() < zs ) continue;
  //
  //  EBDetId ebId( iRHit->id() );
  //  pos = caloGeom->getPosition( ebId );
  //  phi = pos.phi();
  //  eta = pos.eta();

}

bool cand_in_vector( const reco::Candidate* cand, std::vector<const reco::Candidate*> vect ) {

  bool in_vector = false;
  for( unsigned int i = 0; i < vect.size(); i++ ) {
    if( vect[i]->pt() == cand->pt() ) in_vector = true;
  }
  
  return in_vector;
}

bool jet_in_vector( JetRef cand, std::vector<JetRef> vect ) {

  bool in_vector = false;
  for( unsigned int i = 0; i < vect.size(); i++ ) {
    if( vect[i]->pt() == cand->pt() ) in_vector = true;
  }
  
  return in_vector;
}

void ntuplizer::printJetTags(JetRef jet) {

  std::cout << "Tags (pt " << jet->pt() << "):" << std::endl << "b ";
  for( unsigned int i = 0; i < btagnames.size(); i++ ) {
    std::cout << jet->bDiscriminator(btagnames[i]) << " ";
  }
  std::cout << std::endl << "bb ";
  for( unsigned int i = 0; i < bbtagnames.size(); i++ ) {
    std::cout << jet->bDiscriminator(bbtagnames[i]) << " ";
  }
  std::cout << std::endl;

}

std::vector<double> ntuplizer::fillJetTags(JetRef jet) {

  std::vector<double> tags;

  for( unsigned int i = 0; i < btagnames.size(); i++ ) {
    tags.push_back(jet->bDiscriminator(btagnames[i]));
  }
  for( unsigned int i = 0; i < bbtagnames.size(); i++ ) {
    tags.push_back(jet->bDiscriminator(bbtagnames[i]));
  }

  return tags;

}

bool ntuplizer::hasZ4B(const edm::Event& iEvent) {

  edm::Handle<reco::GenParticleCollection> genParticles;
  iEvent.getByToken(genParticleCollectionT_, genParticles);

  bool eventhasZ4B = false;

  for ( unsigned int iG = 0; iG < genParticles->size(); iG++ ) {
    reco::GenParticleRef iGen( genParticles, iG );

    if( iGen->pdgId() == 23 && iGen->status() == 62 ) {
      int thisnb = countBDaughters(iGen);
      if (thisnb == 4) eventhasZ4B = true;
    }
  }
  
  return eventhasZ4B;
}
