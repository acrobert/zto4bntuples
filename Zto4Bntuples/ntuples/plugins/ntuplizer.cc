// --------------------------------------------
//
// Package:    Zto4Bntuples
// Class:      ntuplizer
// 
//
// Author: Andrew C. Roberts
// Started 2022/9/20
// Last Updated 2022/9/21
//
// --------------------------------------------

#include "Zto4Bntuples/ntuples/interface/ntuplizer.h"
#include <stdlib.h>

int nevts;
int n4b, n2b; 
int nHLT;
int totZ;

//
// constructors and destructor
//
ntuplizer::ntuplizer(const edm::ParameterSet& iConfig)
{

  if( AOD ) {

    EBRecHitCollectionT_    = consumes<EcalRecHitCollection>(iConfig.getParameter<edm::InputTag>("reducedAODEBRecHitCollection"));
    EERecHitCollectionT_    = consumes<EcalRecHitCollection>(iConfig.getParameter<edm::InputTag>("reducedAODEERecHitCollection"));
    ESRecHitCollectionT_    = consumes<EcalRecHitCollection>(iConfig.getParameter<edm::InputTag>("reducedAODESRecHitCollection"));
    HBHERecHitCollectionT_ = consumes<HBHERecHitCollection>(iConfig.getParameter<edm::InputTag>("HBHEAODRecHitCollection"));
    pfCollectionT_ = consumes<PFCollection>(iConfig.getParameter<edm::InputTag>("pfAODCollection"));
    
    photonCollectionT_ = consumes<PhotonCollection>(iConfig.getParameter<edm::InputTag>("photonAODCollection"));
    jetCollectionT_ = consumes<JetCollection>(iConfig.getParameter<edm::InputTag>("jetAODCollection"));
    fatJetCollectionT_ = consumes<JetCollection>(iConfig.getParameter<edm::InputTag>("fatJetAODCollection"));
    genParticleCollectionT_ = consumes<reco::GenParticleCollection>(iConfig.getParameter<edm::InputTag>("genAODParticleCollection"));
    vertexCollectionT_ = consumes<reco::VertexCollection>(iConfig.getParameter<edm::InputTag>("vertexAODCollection"));
    //puCollection_ = consumes<std::vector<PileupSummaryInfo>>(iConfig.getParameter<edm::InputTag>());


  } else {
    

    EBRecHitCollectionT_    = consumes<EcalRecHitCollection>(iConfig.getParameter<edm::InputTag>("reducedEBRecHitCollection"));
    EERecHitCollectionT_    = consumes<EcalRecHitCollection>(iConfig.getParameter<edm::InputTag>("reducedEERecHitCollection"));
    ESRecHitCollectionT_    = consumes<EcalRecHitCollection>(iConfig.getParameter<edm::InputTag>("reducedESRecHitCollection"));
    HBHERecHitCollectionT_ = consumes<HBHERecHitCollection>(iConfig.getParameter<edm::InputTag>("HBHERecHitCollection"));
    //HORecHitCollectionT_ = consumes<HORecHitCollection>(iConfig.getParameter<edm::InputTag>("HBHEHFHORecHitCollection"));
    //HFRecHitCollectionT_ = consumes<HFRecHitCollection>(iConfig.getParameter<edm::InputTag>("HBHEHFHORecHitCollection"));
    pfCollectionT_ = consumes<PFCollection>(iConfig.getParameter<edm::InputTag>("pfCollection"));     
    photonCollectionT_ = consumes<PhotonCollection>(iConfig.getParameter<edm::InputTag>("photonCollection"));
    jetCollectionT_ = consumes<JetCollection>(iConfig.getParameter<edm::InputTag>("jetCollection"));
    //jetPuppiCollectionT_ = consumes<JetCollection>(iConfig.getParameter<edm::InputTag>("jetPuppiCollection"));
    fatJetCollectionT_ = consumes<JetCollection>(iConfig.getParameter<edm::InputTag>("fatJetCollection"));
    //fatJetPuppiCollectionT_ = consumes<JetCollection>(iConfig.getParameter<edm::InputTag>("fatJetPuppiCollection"));
    if (!isdata) {
      genParticleCollectionT_ = consumes<reco::GenParticleCollection>(iConfig.getParameter<edm::InputTag>("genParticleCollection"));
    }
    //genJetCollectionT_ = consumes<reco::GenJetCollection>(iConfig.getParameter<edm::InputTag>("genJetCollection"));
    //trackCollectionT_ = consumes<pat::IsolatedTrackCollection>(iConfig.getParameter<edm::InputTag>("trackCollection"));
    //rhoLabel_ = consumes<double>(iConfig.getParameter<edm::InputTag>("rhoLabel"));
    //trgResultsT_ = consumes<edm::TriggerResults>(iConfig.getParameter<edm::InputTag>("trgResults"));
    //genInfoT_ = consumes<GenEventInfoProduct>(iConfig.getParameter<edm::InputTag>("generator"));
    //lheEventT_ = consumes<LHEEventProduct>(iConfig.getParameter<edm::InputTag>("lhe"));
    vertexCollectionT_ = consumes<reco::VertexCollection>(iConfig.getParameter<edm::InputTag>("vertexCollection"));
    puCollection_ = consumes<std::vector<PileupSummaryInfo>>(iConfig.getParameter<edm::InputTag>("pileupCollection"));
    //RECOEBRecHitCollectionT_ = consumes<EcalRecHitCollection>(iConfig.getParameter<edm::InputTag>("EBRecHitCollection"));
    //RECOEERecHitCollectionT_ = consumes<EcalRecHitCollection>(iConfig.getParameter<edm::InputTag>("EERecHitCollection"));
    //RECOESRecHitCollectionT_ = consumes<EcalRecHitCollection>(iConfig.getParameter<edm::InputTag>("ESRecHitCollection"));
    electronCollectionT_ = consumes<ElectronCollection>(iConfig.getParameter<edm::InputTag>("electronCollection"));
    muonCollectionT_ = consumes<MuonCollection>(iConfig.getParameter<edm::InputTag>("muonCollection"));
    trgResultsT_ = consumes<edm::TriggerResults>(iConfig.getParameter<edm::InputTag>("trgResults"));
    METCollectionT_ = consumes<METCollection>(iConfig.getParameter<edm::InputTag>("METCollection"));

  }


  //now do what ever initialization is needed
  usesResource("TFileService");
  edm::Service<TFileService> fs;

  // Output Tree
  EvtTree = fs->make<TTree>("EvtTree", "event tree");
  EvtTree->Branch("eventId", &eventId);
  EvtTree->Branch("runId",   &runId);
  EvtTree->Branch("lumiId",  &lumiId);

  std::cout << "EPid "<< EPid << " Map " << map << " Both " << EPidMap << std::endl;

  //branchesGenParts ( EvtTree, fs );
  if (EPid) {
    branchesObjVectors ( EvtTree, fs );
    branchesJetTags ( EvtTree, fs );
  } else if (map) {
    branchesPFTracks ( EvtTree, fs );
    branchesRecHits ( EvtTree, fs );
    branchesPFCands ( EvtTree, fs );
  } else if (EPidMap) {
    branchesObjVectors ( EvtTree, fs );
    branchesJetTags ( EvtTree, fs );
    branchesPFTracks ( EvtTree, fs );
    branchesRecHits ( EvtTree, fs );
    branchesPFCands ( EvtTree, fs );

  //} else if (tags) {
  //  branchesJetTags ( EvtTree, fs );
  } 
  //branchesBGroups ( EvtTree, fs );
  //branchesECalImages ( EvtTree, fs );
  //branchesHCalImages ( EvtTree, fs );
  //branchesPFTImages ( EvtTree, fs );
  //branchesJets ( EvtTree, fs );

}

ntuplizer::~ntuplizer()
{

  // do anything here that needs to be done at desctruction time
  // (e.g. close files, deallocate resources etc.)

}

//
// member functions
//
// ------------ method called for each event  ------------
void
ntuplizer::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
  using namespace edm;

  eventId = iEvent.id().event();
  runId = iEvent.id().run();
  lumiId = iEvent.id().luminosityBlock();

  if (debug) std::cout << "Event " << eventId << " " << runId << " " << lumiId << std::endl;
  //fillGenParts( iEvent, iSetup );
  //runJetSel( iEvent, iSetup );
  nevts++;

  
  if (EPid) {
    fillObjVectors ( iEvent, iSetup );
    fillJetTags( iEvent, iSetup );
    EvtTree->Fill();
    totZ += nZ;
    if( nZ1b >= 4 ) n4b++;
    if( nZ1b == 2 ) n2b++;
    return;
  } else if (map) {    
    fillPFTracks( iEvent, iSetup );
    fillRecHits( iEvent, iSetup );
    fillPFCands( iEvent, iSetup );
    EvtTree->Fill();
    return;
  } else if (EPidMap) {
    fillJetTags( iEvent, iSetup );
    if (!background & !isdata) {
      fillObjVectors ( iEvent, iSetup );
      totZ += nZ;
      if( nZ1b >= 4 ) n4b++;
      if( nZ1b == 2 ) n2b++;
    }
    //fillPFTracks( iEvent, iSetup );
    //fillRecHits( iEvent, iSetup );
    fillPFCands( iEvent, iSetup );
    EvtTree->Fill();
    return;
  }

  nZ = 0;
  //nevts++;
  save_this_event = false;
  img_eta.clear();
  img_phi.clear();
  img_type.clear();

  bool passed = runZ4BSel ( iEvent, iSetup );
  if( !passed ) return;

  passed = runJetSel ( iEvent, iSetup );
  //passed = run1LSel ( iEvent, iSetup );
  //passed = runGammaSel ( iEvent, iSetup );
  if (passed) nHLT++;
  return;
  fillObjVectors ( iEvent, iSetup );
  fillBGroups ( iEvent, iSetup );

  if( !passed ) return;
  
  if( !save_this_event ) return;

  fillECalImages ( iEvent, iSetup );
  fillHCalImages ( iEvent, iSetup );
  fillPFTImages ( iEvent, iSetup );
  fillJets ( iEvent, iSetup );

  EvtTree->Fill();
}

// ------------ method called once each job just before starting event loop  ------------
void
ntuplizer::beginJob()
{
  // do nothing
}

// ------------ method called once each job just after ending the event loop  ------------
void
ntuplizer::endJob()
{
  std::cout << "Total Zs: " << totZ << " | n Z->4b: " << n4b << " | n groups - duos: " << n2bfj << " trios: " << n3bfj << " quads: " << n4bfj << " | nZ w/ 4 res B's: " << n4resB << std::endl;
  std::cout << "Nevts: " << nevts << " n pass 1L: " << n_pass_1L << " n pass bs " << nhasjets << std::endl;
  //std::cout << "Decays: " << n_decays_e << " " << n_decays_mu << " " << n_decays_tau << std::endl;
  std::cout << n4b << " " << n2b << std::endl;

  //std::cout << "METs: ";
  //for(unsigned int i = 0; i < metvec.size(); i++) {
  //  std::cout << metvec[i] << " ";
  //}
  //std::cout << std::endl;

}

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void
ntuplizer::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  //The following says we do not know what parameters are allowed so do no validation
  // Please change this to state exactly what you do use, even if it is no parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);

  //Specify that only 'tracks' is allowed
  //To use, remove the default given above and uncomment below
  //ParameterSetDescription desc;
  //desc.addUntracked<edm::InputTag>("tracks","ctfWithMaterialTracks");
  //descriptions.addDefault(desc);
}

//define this as a plug-in
DEFINE_FWK_MODULE(ntuplizer);
