
// --------------------------------------------
//
// Package:    Zto4Bntuples
// Class:      ntuplizer
// 
//
// Author: Andrew C. Roberts
// Started 2022/10/5
// Last Updated 2022/10/5
//
// --------------------------------------------

#include "Zto4Bntuples/ntuples/interface/ntuplizer.h"
#include <stdlib.h>

std::vector<std::vector<float>> duo_hcal_img;
std::vector<std::vector<float>> trio_hcal_img;
std::vector<std::vector<float>> quad_hcal_img;

// initialize branches
void ntuplizer::branchesHCalImages ( TTree* tree, edm::Service<TFileService> &fs )
{

  EvtTree->Branch("duo_hcal_img", &duo_hcal_img);
  EvtTree->Branch("trio_hcal_img", &trio_hcal_img);
  EvtTree->Branch("quad_hcal_img", &quad_hcal_img);

}

// fill b group object histograms
void ntuplizer::fillHCalImages(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{

  std::vector<float> hcal_img;

  edm::Handle<HBHERecHitCollection> HBHERecHitsH;
  iEvent.getByToken(HBHERecHitCollectionT_, HBHERecHitsH);
  //edm::Handle<HORecHitCollection> HORecHitsH;
  //iEvent.getByToken(HORecHitCollectionT_, HORecHitsH);
  //edm::Handle<HFRecHitCollection> HFRecHitsH;
  //iEvent.getByToken(HFRecHitCollectionT_, HFRecHitsH);

  edm::ESHandle<CaloGeometry> caloGeomH;
  iSetup.get<CaloGeometryRecord>().get(caloGeomH);
  const CaloGeometry* caloGeom = caloGeomH.product();

  duo_hcal_img.clear();  
  trio_hcal_img.clear();  
  quad_hcal_img.clear();
  
  int ieta, iphi, ieta_crop, iphi_crop, ieta_shift, iphi_shift, idx;
  if( save_this_event ) {
    for( unsigned int iG = 0; iG < img_type.size(); iG++ ) {
      float ctr_eta = img_eta[iG];
      float ctr_phi = img_phi[iG];
      
      hcal_img.assign( hcrop_size*hcrop_size, 0. );
      DetId ctrId( spr::findDetIdHCAL( caloGeom, ctr_eta, ctr_phi, false ) );
      if ( ctrId.subdetId() != HcalBarrel ) continue;
      HcalDetId ctrHdId( ctrId );
      if ( abs(ctrHdId.ieta()) >= HBHE_IETA_MAX_HB ) continue;

      ieta_shift = ctrHdId.ieta() - hcrop_shift;
      iphi_shift = ctrHdId.iphi() - hcrop_shift;

      int ijk = 0;
      std::cout << "Hsize " << HBHERecHitsH->size() << std::endl; //" " << HORecHitsH->size() << " " << HFRecHitsH->size() << std::endl;
      for(HBHERecHitCollection::const_iterator iRHit = HBHERecHitsH->begin();
	  iRHit != HBHERecHitsH->end();
	  ++iRHit) {
	
	ijk++;
	// FIX ZS
	//if ( iRHit->eraw() <= 0 ) continue;
	HcalDetId hdId( iRHit->id() );
	//std::cout << "0.2 " << hdId.subdet() << " " << hdId.ieta() << " " << hdId.iphi() << " " << hdId.rawId() << " " << hdId.depth() << " " << caloGeom->present( hdId ) << std::endl;	
	//std::cout << "Hit " << ijk << " " << iRHit->eraw() << " ieta " << hdId.ieta() << " iphi " << hdId.iphi() << " depth " << hdId.depth();
	//if( caloGeom->present( hdId ) ) {
	//  GlobalPoint pos = caloGeom->getPosition( iRHit->id() );
	//  std::cout << " eta " << pos.eta() << " phi " << pos.phi();
	//}
	//std::cout << std::endl;
	
	ieta = hdId.ieta();
	iphi = hdId.iphi();
	ieta = hdId.ieta() > 0 ? hdId.ieta()-1 : hdId.ieta(); // [-17,...,-1,0,...,16]
	iphi = hdId.iphi()-1; // [0,...,71]
	ieta += EBDetId::MAX_IETA; // [0,...,33]
	
	ieta_crop = ieta - ieta_shift;
	iphi_crop = iphi - iphi_shift;
	if ( iphi_crop >= HBHE_IPHI_MAX ) iphi_crop = iphi_crop - HBHE_IPHI_MAX; // get wrap-around hits                                                                                         
	if ( iphi_crop < 0 ) iphi_crop = iphi_crop + HBHE_IPHI_MAX; // get wrap-around hits                                                                                                          
	
	// select image
	if ( ieta_crop < 0 || ieta_crop > hcrop_size-1 ) continue;
	if ( iphi_crop < 0 || iphi_crop > hcrop_size-1 ) continue;
	
	idx = ieta_crop*hcrop_size + iphi_crop;
	hcal_img[idx] = iRHit->eraw();

      }

      if( img_type[iG] == 2 ) duo_hcal_img.push_back(hcal_img);
      if( img_type[iG] == 3 ) trio_hcal_img.push_back(hcal_img);
      if( img_type[iG] == 4 ) quad_hcal_img.push_back(hcal_img);

    }
  }
}

