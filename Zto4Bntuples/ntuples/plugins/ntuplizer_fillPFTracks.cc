// --------------------------------------------                                                                                                                                                                   
//                                                                                                                                                                                                                
// Package:    Zto4Bntuples                                                                                                                                                                                       
// Class:      ntuplizer                                                                                                                                                                                          
//                                                                                                                                                                                                                
//                                                                                                                                                                                                                
// Author: Andrew C. Roberts                                                                                                                                                                                      
// Started 2022/10/5                                                                                                                                                                                              
// Last Updated 2022/10/5                                                                                                                                                                                        
//                                                                                                                                                                                                               
// --------------------------------------------       

#include "Zto4Bntuples/ntuples/interface/ntuplizer.h"
#include <stdlib.h>

int map_nPFTracks;
std::vector<double> map_PFTracks_pT;
std::vector<double> map_PFTracks_phi;
std::vector<double> map_PFTracks_eta;
std::vector<double> map_PFTracks_d0;
std::vector<double> map_PFTracks_z0;
std::vector<double> map_PFTracks_Q;

void ntuplizer::branchesPFTracks ( TTree* tree, edm::Service<TFileService> &fs )
{

  EvtTree->Branch("map_nPFTracks", &map_nPFTracks);
  EvtTree->Branch("map_PFTracks_pT", &map_PFTracks_pT);
  EvtTree->Branch("map_PFTracks_phi", &map_PFTracks_phi);
  EvtTree->Branch("map_PFTracks_eta", &map_PFTracks_eta);
  EvtTree->Branch("map_PFTracks_d0", &map_PFTracks_d0);
  EvtTree->Branch("map_PFTracks_z0", &map_PFTracks_z0);
  EvtTree->Branch("map_PFTracks_Q", &map_PFTracks_Q);

}

void ntuplizer::fillPFTracks ( const edm::Event& iEvent, const edm::EventSetup& iSetup ) {

  edm::Handle<PFCollection> pfCandsH_;
  iEvent.getByToken( pfCollectionT_, pfCandsH_ );

  // Provides access to global cell position                                                                                                                                                          
  edm::ESHandle<CaloGeometry> caloGeomH_;
  iSetup.get<CaloGeometryRecord>().get( caloGeomH_ );
  const CaloGeometry* caloGeom = caloGeomH_.product();

  edm::Handle<reco::VertexCollection> vertexInfo;
  iEvent.getByToken(vertexCollectionT_, vertexInfo);
  const reco::VertexCollection& vtxs = *vertexInfo;

  map_PFTracks_pT.clear();
  map_PFTracks_phi.clear();
  map_PFTracks_eta.clear();
  map_PFTracks_d0.clear();
  map_PFTracks_z0.clear();
  map_PFTracks_Q.clear();

  if (!hasZ4B(iEvent)) return;
  
  double eta, phi, d0, z0;
  int n = 0;

  for ( PFCollection::const_iterator iPFC = pfCandsH_->begin();
	iPFC != pfCandsH_->end(); ++iPFC ) {

    const reco::Track* thisTrk = iPFC->bestTrack();
    if(!thisTrk) continue;

    const reco::Track thisTrack = *thisTrk;
    double thisTrkPt = thisTrk->pt();
    if (thisTrkPt <= zs) continue;

    double thisTrkQPt = (thisTrk->pt()*thisTrk->charge());
    double thisTrkQ = (double) thisTrk->charge();    

    eta = iPFC->eta();
    phi = iPFC->phi();
    //if ( std::abs(eta) > 2.5 ) continue;

    d0 =  ( !vtxs.empty() ? thisTrk->dxy(vtxs[0].position()) : thisTrk->dxy() );
    z0 =  ( !vtxs.empty() ? thisTrk->dz(vtxs[0].position())  : thisTrk->dz() );

    map_PFTracks_pT.push_back(thisTrkPt);
    map_PFTracks_phi.push_back(phi);
    map_PFTracks_eta.push_back(eta);
    map_PFTracks_d0.push_back(d0);
    map_PFTracks_z0.push_back(z0);
    map_PFTracks_Q.push_back(thisTrkQ);

    n++;
    
  }
  map_nPFTracks = n;
}
