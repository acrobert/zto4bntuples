// --------------------------------------------
//
// Package:    Zto4Bntuples
// Class:      ntuplizer
// 
//
// Author: Andrew C. Roberts
// Started 2023/1/6
// Last Updated 2023/1/6
//
// --------------------------------------------

#include "Zto4Bntuples/ntuples/interface/ntuplizer.h"
#include <stdlib.h>

std::vector<double> friendjet_pfDeepCSVJetTags_probb;
std::vector<double> friendjet_pfDeepCSVJetTags_probbb;
std::vector<double> friendjet_pfDeepFlavourJetTags_probb;
std::vector<double> friendjet_pfDeepFlavourJetTags_probbb;
std::vector<double> friendjet_pfDeepFlavourJetTags_problepb;
std::vector<double> friendjet_pt;
std::vector<double> friendjet_eta;
std::vector<double> friendjet_phi;
std::vector<double> friendjet_energy;
std::vector<double> friendjet_mass;
std::vector<double> friendjet_puId;
std::vector<double> friendjet_puIdDisc;
std::vector<double> friendjet_partonFlavour;
std::vector<double> friendjet_neHEF;
std::vector<double> friendjet_neEmEF;
std::vector<double> friendjet_nConstituents;
std::vector<double> friendjet_jetId;
std::vector<double> friendjet_hadronFlavour;
std::vector<double> friendjet_chHEF;
std::vector<double> friendjet_chEmEF;
//std::vector<double> friendjet_chFPV0EF;
std::vector<double> friendjet_qgl;
std::vector<double> friendjet_charge;
std::vector<double> friendjet_multiplicityMuons;
std::vector<double> friendjet_multiplicityElectrons;
std::vector<double> friendjet_multiplicityPhotons;
std::vector<double> friendjet_multiplicityChargedHad;
std::vector<double> friendjet_multiplicityNeutralHad;
std::vector<double> friendjet_multiplicityHFHad;
std::vector<double> friendjet_multiplicityHFEM;
std::vector<double> friendjet_multiplicityCharged;
std::vector<double> friendjet_multiplicityNeutral;

// initialize branches                                                                                                                                          
void ntuplizer::branchesJetTags ( TTree* tree, edm::Service<TFileService> &fs )
{

  EvtTree->Branch("friendjet_pfDeepCSVJetTags_probb", &friendjet_pfDeepCSVJetTags_probb);
  EvtTree->Branch("friendjet_pfDeepCSVJetTags_probbb", &friendjet_pfDeepCSVJetTags_probbb);
  EvtTree->Branch("friendjet_pfDeepFlavourJetTags_probb", &friendjet_pfDeepFlavourJetTags_probb);
  EvtTree->Branch("friendjet_pfDeepFlavourJetTags_probbb", &friendjet_pfDeepFlavourJetTags_probbb);
  EvtTree->Branch("friendjet_pfDeepFlavourJetTags_problepb", &friendjet_pfDeepFlavourJetTags_problepb);
  EvtTree->Branch("friendjet_pt", &friendjet_pt);
  EvtTree->Branch("friendjet_eta", &friendjet_eta);
  EvtTree->Branch("friendjet_phi", &friendjet_phi);
  EvtTree->Branch("friendjet_energy", &friendjet_energy);
  EvtTree->Branch("friendjet_mass", &friendjet_mass);
  EvtTree->Branch("friendjet_puId", &friendjet_puId);
  EvtTree->Branch("friendjet_puIdDisc", &friendjet_puIdDisc);
  EvtTree->Branch("friendjet_partonFlavour", &friendjet_partonFlavour);
  EvtTree->Branch("friendjet_hadronFlavour", &friendjet_hadronFlavour);
  EvtTree->Branch("friendjet_neHEF", &friendjet_neHEF);
  EvtTree->Branch("friendjet_neEmEF", &friendjet_neEmEF);
  EvtTree->Branch("friendjet_chHEF", &friendjet_chHEF);
  EvtTree->Branch("friendjet_chEmEF", &friendjet_chEmEF);
  EvtTree->Branch("friendjet_nConstituents", &friendjet_nConstituents);
  EvtTree->Branch("friendjet_jetId", &friendjet_jetId);
  //EvtTree->Branch("friendjet_chFPV0EF", &friendjet_chFPV0EF);
  EvtTree->Branch("friendjet_gql", &friendjet_qgl);
  EvtTree->Branch("friendjet_charge", &friendjet_charge);
  EvtTree->Branch("friendjet_multiplicityMuons", &friendjet_multiplicityMuons);
  EvtTree->Branch("friendjet_multiplicityElectrons", &friendjet_multiplicityElectrons);
  EvtTree->Branch("friendjet_multiplicityPhotons", &friendjet_multiplicityPhotons);
  EvtTree->Branch("friendjet_multiplicityChargedHad", &friendjet_multiplicityChargedHad);
  EvtTree->Branch("friendjet_multiplicityNeutralHad", &friendjet_multiplicityNeutralHad);
  EvtTree->Branch("friendjet_multiplicityHFHad", &friendjet_multiplicityHFHad);
  EvtTree->Branch("friendjet_multiplicityHFEM", &friendjet_multiplicityHFEM);
  EvtTree->Branch("friendjet_multiplicityCharged", &friendjet_multiplicityCharged);
  EvtTree->Branch("friendjet_multiplicityNeutral", &friendjet_multiplicityNeutral);


}


void ntuplizer::fillJetTags(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{

  friendjet_pfDeepCSVJetTags_probb.clear();
  friendjet_pfDeepCSVJetTags_probbb.clear();
  friendjet_pfDeepFlavourJetTags_probb.clear();
  friendjet_pfDeepFlavourJetTags_probbb.clear();
  friendjet_pfDeepFlavourJetTags_problepb.clear();
  friendjet_pt.clear();
  friendjet_eta.clear();
  friendjet_phi.clear();
  friendjet_energy.clear();
  friendjet_mass.clear();
  friendjet_puId.clear();
  friendjet_puIdDisc.clear();
  friendjet_partonFlavour.clear();
  friendjet_neHEF.clear();
  friendjet_neEmEF.clear();
  friendjet_nConstituents.clear();
  friendjet_jetId.clear();
  friendjet_hadronFlavour.clear();
  friendjet_chHEF.clear();
  friendjet_chEmEF.clear();
  //friendjet_chFPV0EF.clear();
  friendjet_qgl.clear();
  friendjet_charge.clear();
  friendjet_multiplicityMuons.clear();
  friendjet_multiplicityElectrons.clear();
  friendjet_multiplicityPhotons.clear();
  friendjet_multiplicityChargedHad.clear();
  friendjet_multiplicityNeutralHad.clear();
  friendjet_multiplicityHFHad.clear();
  friendjet_multiplicityHFEM.clear();
  friendjet_multiplicityCharged.clear();
  friendjet_multiplicityNeutral.clear();

  edm::Handle<JetCollection> jets;
  iEvent.getByToken(jetCollectionT_, jets);

  std::vector<edm::Handle<reco::JetTagCollection>> btags;
  for( unsigned int i = 0; i < btagnames.size(); i++ ) {
    edm::Handle<reco::JetTagCollection> tags;
    iEvent.getByLabel(btagnames[i], tags);
    btags.push_back(tags);
  }

  std::vector<edm::Handle<reco::JetTagCollection>> bbtags;
  for( unsigned int i = 0; i < bbtagnames.size(); i++ ) {
    edm::Handle<reco::JetTagCollection> tags;
    iEvent.getByLabel(bbtagnames[i], tags);
    bbtags.push_back(tags);
  }

  int n_bjets;
  int n_bbjets;

  float NHF, NEMF, CHF, MUF, CEMF;
  int NumConst, NumNeutralParticles, CHM;

  for( unsigned int iJ = 0; iJ < jets->size(); iJ++ ) {
    JetRef iJet( jets, iJ );

    friendjet_pfDeepCSVJetTags_probb.push_back(iJet->bDiscriminator(btagnames[0]));
    friendjet_pfDeepCSVJetTags_probbb.push_back(iJet->bDiscriminator(bbtagnames[0]));

    friendjet_pfDeepFlavourJetTags_probb.push_back(iJet->bDiscriminator(btagnames[1]));
    friendjet_pfDeepFlavourJetTags_probbb.push_back(iJet->bDiscriminator(bbtagnames[1]));
    friendjet_pfDeepFlavourJetTags_problepb.push_back(iJet->bDiscriminator(btagnames[2]));

    friendjet_pt.push_back(iJet->pt());
    friendjet_eta.push_back(iJet->eta());
    friendjet_phi.push_back(iJet->phi());
    friendjet_energy.push_back(iJet->energy());
    friendjet_mass.push_back(iJet->mass());

    //std::cout << iJet->jetID() << std::endl;
    //friendjet_jetId.push_back(iJet->jetID());
    
    NHF  = iJet->neutralHadronEnergyFraction();
    NEMF = iJet->neutralEmEnergyFraction();
    CHF  = iJet->chargedHadronEnergyFraction();
    MUF  = iJet->muonEnergyFraction();
    CEMF = iJet->chargedEmEnergyFraction();
    NumConst = iJet->chargedMultiplicity()+iJet->neutralMultiplicity();
    NumNeutralParticles =iJet->neutralMultiplicity();
    CHM      = iJet->chargedMultiplicity(); 

    if (abs(iJet->eta()) < 2.6) {
      if (NHF < 0.9 && NEMF < 0.9 && NumConst > 1 && CHF > 0. && CHM > 0) {
	if (MUF < 0.8 && CEMF < 0.8) {
	  friendjet_jetId.push_back(6);
	} else {
	  friendjet_jetId.push_back(2);
	}
      } else {
	friendjet_jetId.push_back(0); 
      }
    } else if (abs(iJet->eta()) > 2.6 && abs(iJet->eta()) < 2.7) {
      if (NHF < 0.9 && NEMF < 0.99 && CHM > 0) {
	if (MUF < 0.8 && CEMF < 0.8) {
          friendjet_jetId.push_back(6);
        } else {
          friendjet_jetId.push_back(2);
	}
      }else {
	friendjet_jetId.push_back(0);
      }
    } else if (abs(iJet->eta()) > 2.7 && abs(iJet->eta()) < 3.0) {
      if (NEMF > 0.01 && NEMF < 0.99 && NumNeutralParticles > 1) {
	friendjet_jetId.push_back(2);
      }else {
        friendjet_jetId.push_back(0);
      }
    } else if (abs(iJet->eta()) > 3.0 && abs(iJet->eta()) < 5.0) {
      if (NHF > 0.2 && NEMF < 0.9 && NumNeutralParticles > 10) {
        friendjet_jetId.push_back(2);
      }else {
        friendjet_jetId.push_back(0);
      }
    }

    friendjet_partonFlavour.push_back(iJet->partonFlavour());
    friendjet_hadronFlavour.push_back(iJet->hadronFlavour());
    friendjet_chEmEF.push_back(iJet->chargedEmEnergyFraction());
    friendjet_neEmEF.push_back(iJet->neutralEmEnergyFraction());
    friendjet_neHEF.push_back(iJet->neutralHadronEnergyFraction());
    friendjet_chHEF.push_back(iJet->chargedHadronEnergyFraction());
    friendjet_nConstituents.push_back(iJet->nConstituents());
    friendjet_puId.push_back(iJet->userInt("pileupJetId:fullId"));
    friendjet_puIdDisc.push_back(iJet->userFloat("pileupJetId:fullDiscriminant"));
    friendjet_qgl.push_back(iJet->userFloat("QGTagger:qgLikelihood"));

    friendjet_charge.push_back(iJet->jetCharge());
    friendjet_multiplicityMuons.push_back(iJet->muonMultiplicity());
    friendjet_multiplicityElectrons.push_back(iJet->electronMultiplicity());
    friendjet_multiplicityPhotons.push_back(iJet->photonMultiplicity());
    friendjet_multiplicityChargedHad.push_back(iJet->neutralHadronMultiplicity());
    friendjet_multiplicityNeutralHad.push_back(iJet->chargedHadronMultiplicity());
    friendjet_multiplicityHFHad.push_back(iJet->HFHadronMultiplicity());
    friendjet_multiplicityHFEM.push_back(iJet->HFEMMultiplicity());
    friendjet_multiplicityCharged.push_back(iJet->chargedMultiplicity());
    friendjet_multiplicityNeutral.push_back(iJet->neutralMultiplicity());

    if (debug) std::cout << iJet->jetCharge() << " " << iJet->chargedHadronMultiplicity() << " " << iJet->HFEMMultiplicity() << " " <<  iJet->photonMultiplicity() << std::endl;
      
    if (debug) std::cout << "Jet " << iJet->pt() << " " << iJet->eta() << " " << iJet->bDiscriminator(btagnames[1]) << " " << iJet->bDiscriminator(bbtagnames[1]) << std::endl;
  }

}
