// --------------------------------------------                                                                                                                                                                  
//                                                                                                                                                                                                               
// Package:    Zto4Bntuples                                                                                                                                                                                      
// Class:      ntuplizer                                                                                                                                                                                         
//                                                                                                                                                                                                                
//                                                                                                                                                                                                               
// Author: Andrew C. Roberts                                                                                                                                                                                     
// Started 2022/10/5                                                                                                                                                                                             
// Last Updated 2022/10/5                                                                                                                                                                                        
//                                                                                                                                                                                                               
// --------------------------------------------       

#include "Zto4Bntuples/ntuples/interface/ntuplizer.h"
#include <stdlib.h>

int map_nRecHits;
std::vector<double> map_RecHits_E;
std::vector<double> map_RecHits_phi;
std::vector<double> map_RecHits_eta;

void ntuplizer::branchesRecHits ( TTree* tree, edm::Service<TFileService> &fs )
{

  EvtTree->Branch("map_nRecHits",    &map_nRecHits);
  EvtTree->Branch("map_RecHits_E",   &map_RecHits_E);
  EvtTree->Branch("map_RecHits_phi", &map_RecHits_phi);
  EvtTree->Branch("map_RecHits_eta", &map_RecHits_eta);

}

void ntuplizer::fillRecHits ( const edm::Event& iEvent, const edm::EventSetup& iSetup ) {

  edm::Handle<reco::VertexCollection> vertexInfo;
  iEvent.getByToken(vertexCollectionT_, vertexInfo);
  const reco::VertexCollection& vtxs = *vertexInfo;

  edm::Handle<EcalRecHitCollection> EBRecHitsH;
  iEvent.getByToken(EBRecHitCollectionT_, EBRecHitsH);

  edm::ESHandle<CaloGeometry> caloGeomH;
  iSetup.get<CaloGeometryRecord>().get(caloGeomH);
  const CaloGeometry* caloGeom = caloGeomH.product();

  map_RecHits_E.clear();
  map_RecHits_phi.clear();
  map_RecHits_eta.clear();

  if (!hasZ4B(iEvent)) return;

  int ieta, iphi;
  float eta, phi;
  int n = 0;

  for(EcalRecHitCollection::const_iterator iRHit = EBRecHitsH->begin();
      iRHit != EBRecHitsH->end(); ++iRHit) {

    if ( iRHit->energy() <= zs ) continue;
    
    EBDetId ebId( iRHit->id() );
    //auto pos = caloGeom->getPosition( ebId );

    iphi = ebId.iphi()-1; // [0,...,359]                                                                                                                                                                
    ieta = ebId.ieta() > 0 ? ebId.ieta()-1 : ebId.ieta(); // [-85,...,-1,0,...,84]                                                                                                                      
    //ieta += EBDetId::MAX_IETA; // [0,...,169]

    phi = 2*M_PI*(double(iphi)+0.5)/360.;
    if (phi > M_PI) phi = phi - 2*M_PI;
    eta = (double(ieta)+0.5)*1.4702;

    map_RecHits_E.push_back(iRHit->energy());
    map_RecHits_phi.push_back(phi);
    map_RecHits_eta.push_back(eta);
    
    n++;

  }
  map_nRecHits = n;
}
