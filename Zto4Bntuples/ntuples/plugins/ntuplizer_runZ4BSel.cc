
// --------------------------------------------
//
// Package:    Zto4Bntuples
// Class:      ntuplizer
// 
//
// Author: Andrew C. Roberts
// Started 2022/9/30
// Last Updated 2022/10/1
//
// --------------------------------------------

#include "Zto4Bntuples/ntuples/interface/ntuplizer.h"
#include <stdlib.h>

// Find Z-4B event
bool ntuplizer::runZ4BSel(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
  edm::Handle<reco::GenParticleCollection> genParticles;
  iEvent.getByToken(genParticleCollectionT_, genParticles);

  edm::Handle<ElectronCollection> electrons;
  iEvent.getByToken(electronCollectionT_, electrons);

  int nb;
  int nZ4B = 0;
  std::vector<reco::GenParticleRef> evtZs;
  std::vector<int> evtNbs;

  bool hasW = false;;
  reco::GenParticleRef evtW;
  for ( unsigned int iG = 0; iG < genParticles->size(); iG++ ) {
    reco::GenParticleRef iGen( genParticles, iG );

    if( abs(iGen->pdgId()) == 24 && iGen->status() == 62 ) {
      evtW = iGen;
      hasW = true;
    }

    if( iGen->pdgId() == 23 && iGen->status() == 62 ) {
      nZ++;
      //std::cout << "Found Z " << iGen->pt() << " " << iGen->numberOfDaughters() << std::endl;
      nb = countBDaughters(iGen);
      //if (nb > 0) {
      //  std::cout << "Found Z->Bs: " << nb << std::endl;
      //  printProgeny(iGen);
      //}
      //evtNbs.push_back(nb);
      //evtZs.push_back(iGen);
      if (nb == 3) std::cout << "Z " << iG << " bs " << nb << std::endl;
      if (nb == 4) nZ4B++; 
      if (debug) std::cout << "Z " << iG << " bs " << nb << std::endl;

      //if( nb == 4 ) {
      //	if( nZ4B == 1) {
      //	  nZ4B++;
      //	  Z2 = iGen;
      //	  std::cout << "2x Z-> 4B! " << countBDaughters(Z1) << " " << Z1->pt() << " " << nb << " " << Z2->pt() << std::endl;
      //	} else if( nZ4B == 0 ) {
      //	  nZ4B++;
      //	  Z1 = iGen;
      //	}
      //}
      //
      //if( nb == 2 && nZ4B < 2 && nZ > 1 ) {
      //	Z2 = iGen;
      //}

    }
  }

  if (evtZs.size() == 1) {
    Z1 = evtZs[0];
    nZ1b = evtNbs[0];
  } else if (evtZs.size() == 2) {
    if (evtNbs[0] == 4 && evtNbs[1] == 2) {
      Z1 = evtZs[0];
      Z2 = evtZs[1];
      nZ1b = evtNbs[0];
      nZ2b = evtNbs[1];
    } else if (evtNbs[0] == 2 && evtNbs[1] == 4) {
      Z1 = evtZs[1];
      Z2 = evtZs[0];
      nZ1b = evtNbs[1];
      nZ2b = evtNbs[0];
    } else {
      Z1 = evtZs[0];
      Z2 = evtZs[1];
      nZ1b = evtNbs[0];
      nZ2b = evtNbs[1];
    } 
  }

  bool edecay = false;
  const reco::Candidate* edaug;
  if (hasW) {
    for ( unsigned int i = 0; i < evtW->numberOfDaughters(); i++) {
      if (abs(evtW->daughter(i)->pdgId()) == 11 && abs(evtW->daughter(i)->eta()) < 3.) {
	edecay = true;
	n_decays_e++;
	//4std::cout << "E decay " << evtW->daughter(i)->pt() << " " << evtW->daughter(i)->eta() << " " << std::endl;
	edaug = evtW->daughter(i);
      }
      if (abs(evtW->daughter(i)->pdgId()) == 13 && abs(evtW->daughter(i)->eta()) < 3.) {
	n_decays_mu++;
      }
      if (abs(evtW->daughter(i)->pdgId()) == 15 && abs(evtW->daughter(i)->eta()) < 3.) {
	n_decays_tau++;
      }
    }
  }

  bool matched = false;
  ElectronRef matchedE;
  for ( unsigned int iE = 0; iE < electrons->size(); iE++ ) {
    if (!edecay) break;
    ElectronRef iEle( electrons, iE );
    //std::cout << "reco E " << iEle->pt() << " " << iEle->eta() << std::endl;
    
    float dR = reco::deltaR(iEle->eta(), iEle->phi(), edaug->eta(), edaug->phi());
    if (dR < 0.08) {
      matchedE = iEle;
      matched = true;
    }
  }
  matched = false;

  if (matched) std::cout << "E matched " << matchedE->pt() << " " << matchedE->eta() << std::endl;

  if (debug) std::cout << "Z1: " << countBDaughters(Z1) << " " << Z1->pt() << std::endl;//<< " " << countBDaughters(Z2) << " " << Z2->pt() << std::endl;

  if( nZ4B == 0 ) return false; //no Z with 4bs
  if (nZ == 2) std::cout << "Found Z-> 4B in ZZ! " << countBDaughters(Z1) << " " << Z1->pt() << " " << countBDaughters(Z2) << " " << Z2->pt() << std::endl;
  if (nZ == 1) std::cout << "Found Z-> 4B in WZ! " << countBDaughters(Z1) << " " << Z1->pt() << std::endl;
  return true;

}
