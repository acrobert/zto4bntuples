// --------------------------------------------
//
// Package:    Zto4Bntuples
// Class:      ntuplizer
// 
//
// Author: Andrew C. Roberts
// Started 2022/9/20
// Last Updated 2022/10/4
//
// --------------------------------------------

#include "Zto4Bntuples/ntuples/interface/ntuplizer.h"
#include <stdlib.h>

std::vector<std::vector<double>> duo_eta;
std::vector<std::vector<double>> duo_phi;
std::vector<std::vector<double>> duo_rad;
std::vector<std::vector<double>> trio_eta;
std::vector<std::vector<double>> trio_phi;
std::vector<std::vector<double>> trio_rad;
std::vector<std::vector<double>> quad_eta;
std::vector<std::vector<double>> quad_phi;
std::vector<std::vector<double>> quad_rad;

double group_rad = 0.6;
double iso_rad = 2*group_rad;

// initialize branches
void ntuplizer::branchesBGroups ( TTree* tree, edm::Service<TFileService> &fs )
{

  EvtTree->Branch("duo_eta",  &duo_eta);
  EvtTree->Branch("duo_phi",  &duo_phi);
  EvtTree->Branch("duo_rad",  &duo_rad);
  EvtTree->Branch("trio_eta", &trio_eta);
  EvtTree->Branch("trio_phi", &trio_phi);
  EvtTree->Branch("trio_rad", &trio_rad);
  EvtTree->Branch("quad_eta", &quad_eta);
  EvtTree->Branch("quad_phi", &quad_phi);
  EvtTree->Branch("quad_rad", &quad_rad);

}

// fill b group object histograms
void ntuplizer::fillBGroups(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{

  duo_eta.clear();
  duo_phi.clear();
  duo_rad.clear();
  trio_eta.clear();
  trio_phi.clear();
  trio_rad.clear();
  quad_eta.clear();
  quad_phi.clear();
  quad_rad.clear();

  std::vector<double> set_duo_eta;
  std::vector<double> set_duo_phi;
  std::vector<double> set_duo_rad;
  std::vector<double> set_trio_eta;
  std::vector<double> set_trio_phi;
  std::vector<double> set_trio_rad;
  std::vector<double> set_quad_eta;
  std::vector<double> set_quad_phi;
  std::vector<double> set_quad_rad;

  for( unsigned int iZ = 0; iZ < 2; iZ++ ) {
    reco::GenParticleRef iZed = ( iZ == 0 ? Z1 : Z2 );
    int nbdaughters = (iZ == 0 ? nZ1b : nZ2b );

    // find duos/trios
    if( nbdaughters == 4 ) {

      const reco::Candidate* b1;
      const reco::Candidate* b2;
      double dR0, dR1, dR2, ctr_eta, ctr_phi, radius_group;
      std::vector<const reco::Candidate*> finalbs = ( iZ == 0 ? Z1bs : Z2bs );
      std::vector<const reco::Candidate*> otherbs = ( iZ == 0 ? Z2bs : Z1bs );

      for( unsigned int ib = 0; ib < finalbs.size(); ib++ ) {
	for( unsigned int ic = ib + 1; ic < finalbs.size(); ic++ ) {
	  
	  b1 = finalbs[ib];
	  b2 = finalbs[ic];

	  dR0 = reco::deltaR(b1->eta(), b1->phi(), b2->eta(), b2->phi());	  
	  ctr_eta = ( b1->eta() + b2->eta() ) / 2.;
	  ctr_phi = ( b1->phi() + b2->phi() ) / 2.;
	  if ( abs(ctr_eta) > (1.479 - 0.4) ) continue;
	
	  // find duos
	  if( dR0/2. < group_rad ) {
	    
	    bool skip = false;
	    // want isolated duo
	    for( unsigned int id = 0; id < 2 + otherbs.size(); id++ ) {
	      const reco::Candidate* thisb;
	      std::vector<int> idxs {0, 1, 2, 3};
	      std::remove(idxs.begin(), idxs.end(), ib);
	      std::remove(idxs.begin(), idxs.end(), ic);
	      
	      if( id == 0 ) thisb = finalbs[idxs[0]];
	      if( id == 1 ) thisb = finalbs[idxs[1]];
	      if( id >= 2 ) thisb = otherbs[id-2];

	      if( reco::deltaR(ctr_eta, ctr_phi, thisb->eta(), thisb->phi() ) < iso_rad ) {
		skip = true;
		//std::cout << "Not a duo: " << ib << " " << ic << " rad " << dR0/2. << " id " << ( id < 2 ? idxs[id] : id+10 ) << " " <<
		// reco::deltaR( ctr_eta, ctr_phi, thisb->eta(), thisb->phi() ) << " " << std::endl;
		break;
	      }
	    }

	    if( skip ) continue;

	    set_duo_eta.push_back( ctr_eta );
	    set_duo_phi.push_back( ctr_phi );
	    set_duo_rad.push_back( dR0 / 2. );

	    std::cout << "Found duo: " << ib << " " << ic << " radius: " << dR0/2. << " eta: " << ctr_eta << std::endl;
	    n2bfj++;
	    save_this_event = true;
	    img_eta.push_back( ctr_eta );
	    img_phi.push_back( ctr_phi );
	    img_type.push_back( 2 );
	  }
	}
      }

      //std::sort(bdR.begin(), bdR.end());
      //for( unsigned int i = 0; i < 6; i++ ) {
      //	std::cout << bdR[i] << " ";
      //}
      //std::cout << std::endl;
            
      for( unsigned int ib = 0; ib < finalbs.size(); ib++ ) {
	std::vector<int> idxs {0, 1, 2, 3};
	std::remove(idxs.begin(), idxs.end(), ib);

	dR0 = reco::deltaR(finalbs[idxs[0]]->eta(), finalbs[idxs[0]]->phi(), finalbs[idxs[1]]->eta(), finalbs[idxs[1]]->phi());
	dR1 = reco::deltaR(finalbs[idxs[1]]->eta(), finalbs[idxs[1]]->phi(), finalbs[idxs[2]]->eta(), finalbs[idxs[2]]->phi());
	dR2 = reco::deltaR(finalbs[idxs[0]]->eta(), finalbs[idxs[0]]->phi(), finalbs[idxs[2]]->eta(), finalbs[idxs[2]]->phi());
	ctr_eta = ( finalbs[idxs[0]]->eta() + finalbs[idxs[1]]->eta() + finalbs[idxs[2]]->eta() ) / 3.;
	ctr_phi = ( finalbs[idxs[0]]->phi() + finalbs[idxs[1]]->phi() + finalbs[idxs[2]]->phi() ) / 3.;
	radius_group = std::max( { reco::deltaR(finalbs[idxs[0]]->eta(), finalbs[idxs[0]]->phi(), ctr_eta, ctr_phi),
	                           reco::deltaR(finalbs[idxs[1]]->eta(), finalbs[idxs[1]]->phi(), ctr_eta, ctr_phi),
	                           reco::deltaR(finalbs[idxs[2]]->eta(), finalbs[idxs[2]]->phi(), ctr_eta, ctr_phi) });
	std::vector<double> radius_vec = { reco::deltaR(finalbs[idxs[0]]->eta(), finalbs[idxs[0]]->phi(), ctr_eta, ctr_phi),
					   reco::deltaR(finalbs[idxs[1]]->eta(), finalbs[idxs[1]]->phi(), ctr_eta, ctr_phi),
					   reco::deltaR(finalbs[idxs[2]]->eta(), finalbs[idxs[2]]->phi(), ctr_eta, ctr_phi) };

	if ( (abs(ctr_eta) > (1.479 - 0.4)) ) continue;
	
	if( radius_group < group_rad ) {
	  
	  bool skip = false;
	  // want isolated trio
	  for( unsigned int id = 0; id < 1 + otherbs.size(); id++ ) {
	    const reco::Candidate* thisb;
	    if( id == 0 ) thisb = finalbs[ib];
	    if( id >= 1 ) thisb = otherbs[id-1];
	    
	    if( reco::deltaR(ctr_eta, ctr_phi, thisb->eta(), thisb->phi() ) < iso_rad ) {
	      skip = true;
	      //std::cout << "Z " << iZ << " not a trio: not " << ib << " rad: " << radius_group << " id " << id << " " <<
	      //  reco::deltaR( ctr_eta, ctr_phi, thisb->eta(), thisb->phi() ) << " " << std::endl;
	    }
	  }
	
	  if( skip ) continue;


	  set_trio_eta.push_back( ctr_eta );
          set_trio_phi.push_back( ctr_phi );
	  set_trio_rad.push_back( radius_group );
	  //std::cout << "rad vector " << radius_vec[0] << " " << radius_vec[1] << " " << radius_vec[2] << std::endl;
	  std::cout << "Z " << iZ << " Found trio: " << idxs[0] << " " << idxs[1] << " " << idxs[2] << " dR: " << dR0 << " " << dR1 << " " << dR2 << " radius: " << radius_group << " eta: " << ctr_eta << std::endl;
	  n3bfj++;
	  save_this_event = true;
	  img_eta.push_back( ctr_eta );
	  img_phi.push_back( ctr_phi );
	  img_type.push_back( 3 );
	}
      }
      
      //find quad
      ctr_eta = ( finalbs[0]->eta() + finalbs[1]->eta() + finalbs[2]->eta() + finalbs[3]->eta() ) / 4.;
      ctr_phi = ( finalbs[0]->phi() + finalbs[1]->phi() + finalbs[2]->phi() + finalbs[3]->phi() ) / 4.;
      radius_group = std::max( { reco::deltaR(finalbs[0]->eta(), finalbs[0]->phi(), ctr_eta, ctr_phi),
	                         reco::deltaR(finalbs[1]->eta(), finalbs[1]->phi(), ctr_eta, ctr_phi),
	                         reco::deltaR(finalbs[2]->eta(), finalbs[2]->phi(), ctr_eta, ctr_phi),
	                         reco::deltaR(finalbs[3]->eta(), finalbs[3]->phi(), ctr_eta, ctr_phi) });

      std::cout << "Z quad radius " << radius_group << std::endl;
      if ( abs(ctr_eta) > (1.479 - 0.4) ) continue;

      if( radius_group < group_rad ) {

	// want isolated quad
	bool skip = false;
	for( unsigned int id = 0; id < otherbs.size(); id++ ) {
	  const reco::Candidate* thisb = otherbs[id];
	  
	  if( reco::deltaR(ctr_eta, ctr_phi, thisb->eta(), thisb->phi() ) < iso_rad ) {
	    skip = true;
	    //std::cout << "Z " << iZ << " not a quad: id " << id << " " <<
	    //  reco::deltaR( ctr_eta, ctr_phi, thisb->eta(), thisb->phi() ) << " " << std::endl;
	  }
	}	

	if( skip ) continue;
	
	set_quad_eta.push_back( ctr_eta );
	set_quad_phi.push_back( ctr_phi );
	set_quad_rad.push_back( radius_group );
	std::cout << "Z " << iZ << " Found quad, radius: " << radius_group << " eta: " << ctr_eta << std::endl;
	n4bfj++;
	save_this_event = true;
	img_eta.push_back( ctr_eta );
	img_phi.push_back( ctr_phi );
	img_type.push_back( 4 );
      }
    }
  }

  save_this_event = true;

  duo_eta.push_back(set_duo_eta);
  duo_phi.push_back(set_duo_phi);
  duo_rad.push_back(set_duo_rad);
  
  trio_eta.push_back(set_trio_eta);
  trio_phi.push_back(set_trio_phi);
  trio_rad.push_back(set_trio_rad);
  
  quad_eta.push_back(set_quad_eta);
  quad_phi.push_back(set_quad_phi);
  quad_rad.push_back(set_quad_rad);
}

