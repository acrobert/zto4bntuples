// --------------------------------------------
//
// Package:    Zto4Bntuples
// Class:      ntuplizer
// 
//
// Author: Andrew C. Roberts
// Started 2022/10/5
// Last Updated 2022/10/5
//
// --------------------------------------------

#include "Zto4Bntuples/ntuples/interface/ntuplizer.h"
#include <stdlib.h>

std::vector<double> jet_E;
std::vector<double> jet_pT;
std::vector<double> jet_eta;
std::vector<double> jet_phi;
std::vector<std::vector<double>> jet_btags;
std::vector<double> fatJet_E;
std::vector<double> fatJet_pT;
std::vector<double> fatJet_eta;
std::vector<double> fatJet_phi;
std::vector<std::vector<double>> fatJet_btags;

// initialize branches
void ntuplizer::branchesJets ( TTree* tree, edm::Service<TFileService> &fs )
{

  EvtTree->Branch("jet_E", &jet_E);
  EvtTree->Branch("jet_pT", &jet_pT);
  EvtTree->Branch("jet_eta", &jet_eta);
  EvtTree->Branch("jet_phi", &jet_phi);
  EvtTree->Branch("jet_btags", &jet_btags);
  EvtTree->Branch("fatJet_E", &fatJet_E);
  EvtTree->Branch("fatJet_pT", &fatJet_pT);
  EvtTree->Branch("fatJet_eta", &fatJet_eta);
  EvtTree->Branch("fatJet_phi", &fatJet_phi);

}

//std::vector<std::string> btagnames{"pfDeepCSVJetTags:probb", "pfDeepCMVAJetTags:probb", "pfDeepFlavourJetTags:probb", "pfDeepCombinedJetTags:probb", "pfCombinedSecondaryVertexV2BJetTags", "combinedSecondaryVertexV2BJetTags", "pfCombinedInclusiveSecondaryVertexV2BJetTags", "pfCombinedMVAV2BJetTags"};
//std::vector<std::string> btagnames{"pfDeepCSVJetTags:probb", "pfDeepFlavourJetTags:probb", "pfCombinedSecondaryVertexV2BJetTags", "pfCombinedInclusiveSecondaryVertexV2BJetTags", "pfCombinedMVAV2BJetTags"};

//std::vector<std::string> bbtagnames{"pfDeepCSVJetTags:probbb", "pfNegativeDeepCSVJetTags:probbb", "pfPositiveDeepCSVJetTags:probbb", "pfDeepCMVAJetTags:probbb", "pfNegativeDeepCMVAJetTags:probbb", "pfPositiveDeepCMVAJetTags:probbb", "pfDeepFlavourJetTags:probbb", "pfNegativeDeepFlavourJetTags:probbb", "pfPositiveDeepFlavourJetTags:probbb"};
//std::vector<std::string> bbtagnames{"pfDeepCSVJetTags:probbb", "pfDeepFlavourJetTags:probbb"};

// fill object 4-vector histograms
void ntuplizer::fillJets (const edm::Event& iEvent, const edm::EventSetup& iSetup)
{

  jet_E.clear();
  jet_pT.clear();
  jet_eta.clear();
  jet_phi.clear();
  jet_btags.clear();
  fatJet_E.clear();
  fatJet_pT.clear();
  fatJet_eta.clear();
  fatJet_phi.clear();

  //std::vector<double> evt_jet_E;
  //std::vector<double> evt_jet_pT;
  //std::vector<double> evt_jet_eta;
  //std::vector<double> evt_jet_phi;
  //std::vector<std::vector<double>> evt_jet_btags;
  //std::vector<double> evt_fatJet_E;
  //std::vector<double> evt_fatJet_pT;
  //std::vector<double> evt_fatJet_eta;
  //std::vector<double> evt_fatJet_phi;
  //std::vector<std::vector<double>> evt_fatJet_btags;

  edm::Handle<JetCollection> jets;
  iEvent.getByToken(jetCollectionT_, jets);

  edm::Handle<JetCollection> fatJets;
  iEvent.getByToken(fatJetCollectionT_, fatJets);

  std::vector<edm::Handle<reco::JetTagCollection>> btags;
  for( unsigned int i = 0; i < btagnames.size(); i++ ) {
    edm::Handle<reco::JetTagCollection> tags;
    iEvent.getByLabel(btagnames[i], tags);
    btags.push_back(tags);
  }

  std::vector<edm::Handle<reco::JetTagCollection>> bbtags;
  for( unsigned int i = 0; i < bbtagnames.size(); i++ ) {
    edm::Handle<reco::JetTagCollection> tags;
    iEvent.getByLabel(bbtagnames[i], tags);
    bbtags.push_back(tags);
  }

  //edm::Handle<reco::JetTagCollection> btags0;
  //iEvent.getByLabel("pfCombinedInclusiveSecondaryVertexV2BJetTags", btags0);  
  //
  //edm::Handle<reco::JetTagCollection> btags1;
  //iEvent.getByLabel("pfJetProbabilityBJetTags", btags1);  
  //
  //edm::Handle<reco::JetTagCollection> bbtags;
  //iEvent.getByLabel("pfDeepCSVJetTags:probb", bbtags);  

  for( unsigned int iJ = 0; iJ < jets->size(); iJ++ ) {
    
    JetRef iJet( jets, iJ );
    jet_E.push_back( iJet->energy() );
    jet_pT.push_back( iJet->pt() );
    jet_eta.push_back( iJet->eta() );
    jet_phi.push_back( iJet->phi() );
    //evt_jet_btags.push_back( iJet->bDiscriminator("pfCombinedInclusiveSecondaryVertexV2BJetTags") );    
    jet_btags.push_back(fillJetTags(iJet));
    //printJetTags(iJet);
    
  }

  for( unsigned int iJ = 0; iJ < fatJets->size(); iJ++ ) {
    
    JetRef iJet( fatJets, iJ );
    fatJet_E.push_back( iJet->energy() );
    fatJet_pT.push_back( iJet->pt() );
    fatJet_eta.push_back( iJet->eta() );
    fatJet_phi.push_back( iJet->phi() );
    //std::cout << iJet->pt() << " fj btags " << iJet->bDiscriminator("pfCombinedInclusiveSecondaryVertexV2BJetTags") << " " << iJet->bDiscriminator("pfJetProbabilityBJetTags") << " " << iJet->bDiscriminator("pfDeepCSVJetTags:probb") << std::endl;
    fatJet_btags.push_back(fillJetTags(iJet));
    //printJetTags(iJet);
    
  }
  
  //std::cout << "Tags: " << jet_btags.size() << " " << fatJet_btags.size() << std::endl;

  //jet_E.push_back( evt_jet_E );
  //jet_pT.push_back( evt_jet_pT );
  //jet_eta.push_back( evt_jet_eta );
  //jet_phi.push_back( evt_jet_phi );
  //jet_btags.push_back( evt_jet_btags );
  //fatJet_E.push_back( evt_fatJet_E );
  //fatJet_pT.push_back( evt_fatJet_pT );
  //fatJet_eta.push_back( evt_fatJet_eta );
  //fatJet_phi.push_back( evt_fatJet_phi );
  

  //std::cout << "Z1bs: " << Z1bs.size() << " Z2bs: " << Z2bs.size() << std::endl;
  //for( unsigned int iB = 0; iB < Z1bs.size() + Z2bs.size(); iB++ ) {
  //  const reco::Candidate* b1;
  //  if ( iB < Z1bs.size() ) b1 = Z1bs[iB];
  //  if ( iB >= Z1bs.size() ) b1 = Z2bs[iB - Z1bs.size()];
  //
  //  std::cout << iB << " " << b1->pt() << " " << b1->eta() << " " << b1->phi() << std::endl;
  //  
  //  int closest_id = -1;
  //  float mindR = 100000000.;
  //  bool matched = false;	
  //  
  //  for( unsigned int iJ = 0; iJ < jets->size(); iJ++ ) {
  //    
  //    JetRef iJet( jets, iJ );
  //    float dR = reco::deltaR(b1->eta(), b1->phi(), iJet->eta(), iJet->phi());
  //    
  //    if( dR < mindR && iJet->bDiscriminator("pfCombinedInclusiveSecondaryVertexV2BJetTags") > 0.3 && !jet_in_vector( iJet, matchedJets ) ) {
  //	mindR = dR;
  //	closest_id = iJ;
  //    }
  //  }
  //  
  //  if( closest_id != -1 && iB < 4 ) {
  //    JetRef closest_jet ( jets, closest_id );
  //    if( mindR < 0.4 ) matchedJets.push_back( closest_jet );
  //    matched = true;
  //    std::cout << "Matched Jet: iJ " << closest_id << " dR " << mindR << " btag " << closest_jet->bDiscriminator("pfCombinedInclusiveSecondaryVertexV2BJetTags") << std::endl;
  //  }
  //
  //  closest_id = -1;
  //  mindR = 100000000.;
  //  matched = false;
  //
  //  for( unsigned int iJ = 0; iJ < fatJets->size(); iJ++ ) {
  //
  //    JetRef iJet( fatJets, iJ );
  //    float dR = reco::deltaR(b1->eta(), b1->phi(), iJet->eta(), iJet->phi());
  //
  //    if( dR < mindR && !jet_in_vector( iJet, matchedJets ) ) {
  //
  //    }
  //
  //  }
  //  
  //
  //
  //    } else if( iJ >= jets->size() ) {
  //	JetRef iJet( fatJets, iJ - jets->size() );
  //	float dR = reco::deltaR(b1->eta(), b1->phi(), iJet->eta(), iJet->phi());
  //
  //	if( dR < 0.4 ) {
  //	  if( iJ >= jets->size() ) std::cout << "Matched fatJet: iJ " << iJ << " dR " << dR << std::endl;
  //	}
  //    }
  //    if( matched ) break;
  //  }
  //  if( matched && iB < 4 ) nmatched++;
  //}

  for( unsigned int iG = 0; iG < img_type.size(); iG++ ) {
    for( unsigned int iJ = 0; iJ < jets->size() + fatJets->size(); iJ++ ) {
      JetRef iJet;
      if( iJ < jets->size() ) {
	JetRef iJet( jets, iJ );
	float dR = reco::deltaR(img_eta[iG], img_phi[iG], iJet->eta(), iJet->phi());
	if( dR < 1. ) {
	  std::cout << "Jet in group: iJ " << iJ << " rad " << dR << " pT " << iJet->pt() << std::endl;
	}
      } else if( iJ >= jets->size() ) {
	JetRef iJet( fatJets, iJ - jets->size() );
	float dR = reco::deltaR(img_eta[iG], img_phi[iG], iJet->eta(), iJet->phi());
	if( dR < 1. ) {
	  std::cout << "FatJet in group: iJ " << iJ << " rad " << dR << " pT " << iJet->pt() << std::endl;
        }
      }
    }
  }
  
  //std::cout << "nJets: " << jets->size() << std::endl;
  //for( unsigned int iJ = 0; iJ < jets->size(); iJ++ ) {
  //  JetRef iJet( jets, iJ );
  //  //edm::RefToBase<reco::Jet> iJet = jets->refAt(iJ);
  //  std::cout << iJ << " " << iJet->pt() << " " << iJet->eta() << " " << iJet->phi() << " btag = " <<
  //    iJet->bDiscriminator("pfCombinedInclusiveSecondaryVertexV2BJetTags") << " " << 
  //    iJet->bDiscriminator("pfJetProbabilityBJetTags") << " " << 
  //    iJet->bDiscriminator("fDeepCSVJetTags:probb") << std::endl;
  //}


}

