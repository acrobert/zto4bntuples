// --------------------------------------------
//
// Package:    Zto4Bntuples
// Class:      ntuplizer
// 
//
// Author: Andrew C. Roberts
// Started 2023/1/6
// Last Updated 2023/1/6
//
// --------------------------------------------

#include "Zto4Bntuples/ntuples/interface/ntuplizer.h"
#include <stdlib.h>

bool ntuplizer::runGammaSel(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{

  edm::Handle<JetCollection> jets;
  iEvent.getByToken(jetCollectionT_, jets);

  edm::Handle<JetCollection> fatJets;
  iEvent.getByToken(fatJetCollectionT_, fatJets);

  edm::Handle<reco::GenParticleCollection> genParticles;
  iEvent.getByToken(genParticleCollectionT_, genParticles);

  edm::Handle<PhotonCollection> photons;
  iEvent.getByToken(photonCollectionT_, photons);

  edm::Handle<ElectronCollection> electrons;
  iEvent.getByToken(electronCollectionT_, electrons);

  edm::Handle<MuonCollection> muons;  
  iEvent.getByToken(muonCollectionT_, muons);

  edm::Handle<edm::TriggerResults> trgs;
  iEvent.getByToken( trgResultsT_, trgs );

  edm::Handle<METCollection> METs;
  iEvent.getByToken(METCollectionT_, METs);


  std::vector<edm::Handle<reco::JetTagCollection>> btags;
  for( unsigned int i = 0; i < btagnames.size(); i++ ) {
    edm::Handle<reco::JetTagCollection> tags;
    iEvent.getByLabel(btagnames[i], tags);
    btags.push_back(tags);
  }

  std::vector<edm::Handle<reco::JetTagCollection>> bbtags;
  for( unsigned int i = 0; i < bbtagnames.size(); i++ ) {
    edm::Handle<reco::JetTagCollection> tags;
    iEvent.getByLabel(bbtagnames[i], tags);
    bbtags.push_back(tags);
  }

  const edm::TriggerNames &triggerNames = iEvent.triggerNames( *trgs );
  if ( debug ) std::cout << " N triggers:" << trgs->size() << std::endl;
  //for ( unsigned int iT = 0; iT < trgs->size(); iT++ ) {
  //  if ( debug ) std::cout << " name["<<iT<<"]:"<<triggerNames.triggerName(iT)<< std::endl;
  //}

  int hltAccept = -1;
  std::string phoName = "HLT_Photon200_v*";
  std::vector< std::vector<std::string>::const_iterator > phoMatches = edm::regexMatch( triggerNames.triggerNames(), phoName );
  std::string phoName2 = "HLT_Photon110EB__TightID_TightIso_v*";
  std::vector< std::vector<std::string>::const_iterator > phoMatches2 = edm::regexMatch( triggerNames.triggerNames(), phoName2 );
  phoMatches.insert(phoMatches.end(), phoMatches2.begin(), phoMatches2.end());

  //for ( auto const& iT : phoMatches ) {
  //  std::cout << *iT << std::endl;
  //}

  //std::vector< std::vector<std::string>::const_iterator > trgMatches = {"HLT_IsoMu27_v", "HLT_Ele27_WPLoose_Gsf_v"};
  std::vector< std::vector<std::string>::const_iterator > trgMatches = phoMatches;
  //trgMatches.insert(trgMatches.end(), eleMatches.begin(), eleMatches.end());
  if ( debug ) std::cout << " N matches: " << trgMatches.size() << std::endl;
  if ( !trgMatches.empty() ) {

    hltAccept = 0;
    for ( auto const& iT : trgMatches ) {
      if ( debug ) std::cout << " name["<<triggerNames.triggerIndex(*iT)<<"]:"<< *iT << " -> " << trgs->accept(triggerNames.triggerIndex(*iT)) << std::endl;
      if ( trgs->accept(triggerNames.triggerIndex(*iT)) ) {
	hltAccept = 1;
	//std::cout << "Passes HLT " << *iT << std::endl;
      }
      //break;
    }
  }

  if (!hltAccept) return false;

  bool hardPhoton = false;
  
  for ( unsigned int iP = 0; iP < photons->size(); iP++ ) {
    if (hardPhoton) break;

    PhotonRef iPho( photons, iP );
    std::cout << "Pho pt " << iPho->pt() << std::endl;
    if (iPho->pt() >= phoptthresh) {
      hardPhoton = true;
    }
  }
  
  if (!hardPhoton) return false;
  std::cout << "Passed photon selection" << std::endl;
  n_pass_1L++;

  //bool four;
  //bool two_plus_duo;
  //bool one_plus_trio;
  //bool quad;

  int n_bjets = 0;
  int n_bbjets = 0;

  for( unsigned int iJ = 0; iJ < jets->size(); iJ++ ) {
    JetRef iJet( jets, iJ );
    std::vector<double> tags = fillJetTags(iJet);
    //printJetTags(iJet);
    double maxb = tags[1];
    double maxbb = tags[6];
    if (maxb > btagthresh) n_bjets++;
    if (maxbb > bbtagthresh) n_bbjets++;
    //std::cout << "Jet " << iJet->pt() << " " << iJet->eta() << " " << iJet->phi() << " " << maxb << " " << maxbb << std::endl;
  }


  for( unsigned int iJ = 0; iJ < fatJets->size(); iJ++ ) {
    JetRef iJet( fatJets, iJ );
    std::vector<double> tags = fillJetTags(iJet);
    //printJetTags(iJet);
    double maxb = tags[1];//*max_element(tags.begin(), std::next(tags.begin(), 5));
    double maxbb = tags[6];//*max_element(std::next(tags.begin(), 5), tags.end());
    //std::cout << "Max tags " << maxb << " " << maxbb << std::endl;
    if (maxb > btagthresh) n_bjets++;
    if (maxbb > bbtagthresh) n_bbjets++; 
    //std::cout << "FatJet " << iJet->pt() << " " << iJet->eta() << " " << iJet->phi() << " " << maxb << " " << maxbb << std::endl;
  }

  std::cout << "B-tagged jets: " << n_bjets << " " << n_bbjets << std::endl;

  for( unsigned int iM = 0; iM < METs->size(); iM++ ) {
    METRef iMET( METs, iM );
    std::cout << "MET pt: " << iMET->pt() << std::endl;
    metvec.push_back(iMET->pt());
  }

  if ((n_bjets >= 4) || (n_bjets >=1 && n_bbjets >= 1)) {
    nhasjets++;
    return true;
  } else {
    return false;
  }
}
