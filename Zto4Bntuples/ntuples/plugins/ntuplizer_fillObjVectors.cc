// --------------------------------------------
//
// Package:    Zto4Bntuples
// Class:      ntuplizer
// 
//
// Author: Andrew C. Roberts
// Started 2022/9/20
// Last Updated 2022/10/4
//
// --------------------------------------------

#include "Zto4Bntuples/ntuples/interface/ntuplizer.h"
#include <stdlib.h>

std::vector<int> Zs_nB;
std::vector<double> Zs_E;
std::vector<double> Zs_pT;
std::vector<double> Zs_eta;
std::vector<double> Zs_phi;
std::vector<double> Zs_mass;
std::vector<int> Zs_pdgId;

std::vector<std::vector<double>> Zs_orig_Bs_E;
std::vector<std::vector<double>> Zs_orig_Bs_pT;
std::vector<std::vector<double>> Zs_orig_Bs_eta;
std::vector<std::vector<double>> Zs_orig_Bs_phi;
std::vector<std::vector<double>> Zs_orig_Bs_mass;
std::vector<std::vector<double>> Zs_final_Bs_E;
std::vector<std::vector<double>> Zs_final_Bs_pT;
std::vector<std::vector<double>> Zs_final_Bs_eta;
std::vector<std::vector<double>> Zs_final_Bs_phi;
std::vector<std::vector<double>> Zs_final_Bs_mass;

std::vector<std::vector<double>> Zs_orig_Qs_E;
std::vector<std::vector<double>> Zs_orig_Qs_pT;
std::vector<std::vector<double>> Zs_orig_Qs_eta;
std::vector<std::vector<double>> Zs_orig_Qs_phi;
std::vector<std::vector<double>> Zs_orig_Qs_mass;
std::vector<std::vector<int>> Zs_orig_Qs_pdgId;
std::vector<std::vector<double>> Zs_final_Qs_E;
std::vector<std::vector<double>> Zs_final_Qs_pT;
std::vector<std::vector<double>> Zs_final_Qs_eta;
std::vector<std::vector<double>> Zs_final_Qs_phi;
std::vector<std::vector<double>> Zs_final_Qs_mass;
std::vector<std::vector<int>> Zs_final_Qs_pdgId;

std::vector<double> AllTruthBs_energy;
std::vector<double> AllTruthBs_pt;
std::vector<double> AllTruthBs_eta;
std::vector<double> AllTruthBs_phi;
std::vector<double> AllTruthBs_mass;

std::vector<double> NonZTruthBs_energy;
std::vector<double> NonZTruthBs_pt;
std::vector<double> NonZTruthBs_eta;
std::vector<double> NonZTruthBs_phi;
std::vector<double> NonZTruthBs_mass;

std::vector<double> ZTruthBs_energy;
std::vector<double> ZTruthBs_pt;
std::vector<double> ZTruthBs_eta;
std::vector<double> ZTruthBs_phi;
std::vector<double> ZTruthBs_mass;

// initialize branches
void ntuplizer::branchesObjVectors ( TTree* tree, edm::Service<TFileService> &fs )
{

  EvtTree->Branch("Zs_nB", &Zs_nB);

  EvtTree->Branch("Zs_E", &Zs_E);
  EvtTree->Branch("Zs_pT", &Zs_pT);
  EvtTree->Branch("Zs_eta", &Zs_eta);
  EvtTree->Branch("Zs_phi", &Zs_phi);
  EvtTree->Branch("Zs_mass", &Zs_mass);
  EvtTree->Branch("Zs_pdgId", &Zs_pdgId);

  EvtTree->Branch("Zs_orig_Bs_E", &Zs_orig_Bs_E);
  EvtTree->Branch("Zs_orig_Bs_pT", &Zs_orig_Bs_pT);
  EvtTree->Branch("Zs_orig_Bs_eta", &Zs_orig_Bs_eta);
  EvtTree->Branch("Zs_orig_Bs_phi", &Zs_orig_Bs_phi);
  EvtTree->Branch("Zs_orig_Bs_mass", &Zs_orig_Bs_mass);
  EvtTree->Branch("Zs_final_Bs_E", &Zs_final_Bs_E);
  EvtTree->Branch("Zs_final_Bs_pT", &Zs_final_Bs_pT);
  EvtTree->Branch("Zs_final_Bs_eta", &Zs_final_Bs_eta);
  EvtTree->Branch("Zs_final_Bs_phi", &Zs_final_Bs_phi);
  EvtTree->Branch("Zs_final_Bs_mass", &Zs_final_Bs_mass);

  EvtTree->Branch("AllTruthBs_energy", &AllTruthBs_energy);
  EvtTree->Branch("AllTruthBs_pt",     &AllTruthBs_pt);
  EvtTree->Branch("AllTruthBs_eta",    &AllTruthBs_eta);
  EvtTree->Branch("AllTruthBs_phi",    &AllTruthBs_phi);
  EvtTree->Branch("AllTruthBs_mass",   &AllTruthBs_mass);

  EvtTree->Branch("NonZTruthBs_energy", &NonZTruthBs_energy);
  EvtTree->Branch("NonZTruthBs_pt",     &NonZTruthBs_pt);
  EvtTree->Branch("NonZTruthBs_eta",    &NonZTruthBs_eta);
  EvtTree->Branch("NonZTruthBs_phi",    &NonZTruthBs_phi);
  EvtTree->Branch("NonZTruthBs_mass",   &NonZTruthBs_mass);

  EvtTree->Branch("ZTruthBs_energy", &ZTruthBs_energy);
  EvtTree->Branch("ZTruthBs_pt",     &ZTruthBs_pt);
  EvtTree->Branch("ZTruthBs_eta",    &ZTruthBs_eta);
  EvtTree->Branch("ZTruthBs_phi",    &ZTruthBs_phi);
  EvtTree->Branch("ZTruthBs_mass",   &ZTruthBs_mass);

  EvtTree->Branch("Zs_orig_Qs_E", &Zs_orig_Qs_E);
  EvtTree->Branch("Zs_orig_Qs_pT", &Zs_orig_Qs_pT);
  EvtTree->Branch("Zs_orig_Qs_eta", &Zs_orig_Qs_eta);
  EvtTree->Branch("Zs_orig_Qs_phi", &Zs_orig_Qs_phi);
  EvtTree->Branch("Zs_orig_Qs_mass", &Zs_orig_Qs_mass);
  EvtTree->Branch("Zs_orig_Qs_pdgId", &Zs_orig_Qs_pdgId);
  EvtTree->Branch("Zs_final_Qs_E", &Zs_final_Qs_E);
  EvtTree->Branch("Zs_final_Qs_pT", &Zs_final_Qs_pT);
  EvtTree->Branch("Zs_final_Qs_eta", &Zs_final_Qs_eta);
  EvtTree->Branch("Zs_final_Qs_phi", &Zs_final_Qs_phi);
  EvtTree->Branch("Zs_final_Qs_mass", &Zs_final_Qs_mass);
  EvtTree->Branch("Zs_final_Qs_pdgId", &Zs_final_Qs_pdgId);

}

// fill object 4-vector histograms
void ntuplizer::fillObjVectors(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{

  nZ = 0;
  Zs_nB.clear();
  Zs_E.clear();
  Zs_pT.clear();
  Zs_eta.clear();
  Zs_phi.clear();
  Zs_mass.clear();
  Zs_pdgId.clear();
  Zs_orig_Bs_E.clear();
  Zs_orig_Bs_pT.clear();
  Zs_orig_Bs_eta.clear();
  Zs_orig_Bs_phi.clear();  
  Zs_orig_Bs_mass.clear();  
  Zs_final_Bs_E.clear();
  Zs_final_Bs_pT.clear();
  Zs_final_Bs_eta.clear();
  Zs_final_Bs_phi.clear();
  Zs_final_Bs_mass.clear();
  Zs_orig_Qs_E.clear();
  Zs_orig_Qs_pT.clear();
  Zs_orig_Qs_eta.clear();
  Zs_orig_Qs_phi.clear();  
  Zs_orig_Qs_mass.clear();  
  Zs_orig_Qs_pdgId.clear();  
  Zs_final_Qs_E.clear();
  Zs_final_Qs_pT.clear();
  Zs_final_Qs_eta.clear();
  Zs_final_Qs_phi.clear();
  Zs_final_Qs_mass.clear();
  Zs_final_Qs_pdgId.clear();

  AllTruthBs_energy.clear();
  AllTruthBs_pt.clear();
  AllTruthBs_eta.clear();
  AllTruthBs_phi.clear();
  AllTruthBs_mass.clear();

  NonZTruthBs_energy.clear();
  NonZTruthBs_pt.clear();
  NonZTruthBs_eta.clear();
  NonZTruthBs_phi.clear();
  NonZTruthBs_mass.clear();

  ZTruthBs_energy.clear();
  ZTruthBs_pt.clear();
  ZTruthBs_eta.clear();
  ZTruthBs_phi.clear();
  ZTruthBs_mass.clear();


  edm::Handle<reco::GenParticleCollection> genParticles;
  iEvent.getByToken(genParticleCollectionT_, genParticles);

  std::vector<reco::GenParticleRef> evtZs;
  std::vector<reco::GenParticleRef> initProtons;
  std::vector<int> evtNbs;
  std::vector<bool> evtQd;
  if (debug) std::cout << "N gen particles " << genParticles->size() << std::endl;
  for ( unsigned int iG = 0; iG < genParticles->size(); iG++ ) {
    reco::GenParticleRef iGen( genParticles, iG );

    if( iGen->pdgId() == 2212 && iGen->status() == 4 ) {
      initProtons.push_back(iGen);
    }

    //if (iGen->numberOfMothers() > 0) {
    //  std::cout << iG << " " << iGen->pdgId() << " " << iGen->status() << " " << iGen->mother(0)->pdgId() << " " << iGen->pt() << std::endl;
    //} else {
    //  std::cout << iG << " " << iGen->pdgId() << " " << iGen->status() << " " << iGen->pt() << std::endl;
    //}


    //if( iGen->pdgId() == 23 ) {
    //  std::cout << iGen->pdgId() << " " << iGen->pt() << " " << iGen->status() << std::endl;
    //}
    //
    //if( iGen->pdgId() == 25 ) {
    //  std::cout << iGen->pdgId() << " " << iGen->pt() << " " << iGen->status() << std::endl;
    //}

    if( (iGen->pdgId() == 23 || iGen->pdgId() == 25) && iGen->status() == 62 ) {
      nZ++;
      evtZs.push_back(iGen);
      int thisnb = countBDaughters(iGen);
      //std::cout << iGen->pdgId() << " " << iGen->pt() << " " << thisnb << std::endl;
      evtNbs.push_back(thisnb);

      if (thisnb < 4) {
	//std::cout << "Z to not 4B! " << thisnb << std::endl;
	//printProgeny(iGen);
      }
      
      bool qd = false;
      for (unsigned int iD = 0; iD < iGen->numberOfDaughters(); iD++) {
	if (std::abs(iGen->daughter(iD)->pdgId()) <= 5) qd = true;
      }
      evtQd.push_back(qd);

    }
  }

  if (nZ != 1){
    //std::cout << "Did not find one Z! " << nZ << std::endl; 
    
    for ( unsigned int iG = 0; iG < genParticles->size(); iG++ ) {
      reco::GenParticleRef iGen( genParticles, iG );
      if( iGen->pdgId() == 23 ) {
	//std::cout << iGen->pdgId() << " " << iGen->pt() << " " << iGen->status() << std::endl;
	//printProgeny(iGen);
      }
    }
  }

  if (nZ == 1) {
    Z1 = evtZs[0];
    nZ1b = evtNbs[0];
  } else if (nZ == 2) {
    if (evtNbs[0] == 4 && evtNbs[1] == 2) {
      Z1 = evtZs[0];
      Z2 = evtZs[1];
      nZ1b = evtNbs[0];
      nZ2b = evtNbs[1];
    } else if (evtNbs[0] == 2 && evtNbs[1] == 4) {
      Z1 = evtZs[1];
      Z2 = evtZs[0];
      nZ1b = evtNbs[1];
      nZ2b = evtNbs[0];
    } else if (evtQd[0] && !evtQd[1]) {
      Z1 = evtZs[0];
      Z2 = evtZs[1];
      nZ1b = evtNbs[0];
      nZ2b = evtNbs[1];
    } else if (!evtQd[0] && evtQd[1]) {
      Z1 = evtZs[1];
      Z2 = evtZs[0];
      nZ1b = evtNbs[1];
      nZ2b = evtNbs[0];
    } else {
      Z1 = evtZs[0];
      Z2 = evtZs[1];
      nZ1b = evtNbs[0];
      nZ2b = evtNbs[1];
    }
  }

  if (debug) std::cout << "nZ " << nZ << std::endl;

  if (nZ > 0) {
    Zs_nB.push_back(nZ1b);
    Zs_E.push_back(Z1->energy());
    Zs_pT.push_back(Z1->pt());
    Zs_eta.push_back(Z1->eta());
    Zs_phi.push_back(Z1->phi());
    Zs_mass.push_back(Z1->mass());
    Zs_pdgId.push_back(Z1->pdgId());
  }

  if (nZ == 2) {
    Zs_nB.push_back(nZ2b);
    Zs_E.push_back(Z2->energy());
    Zs_pT.push_back(Z2->pt());
    Zs_eta.push_back(Z2->eta());
    Zs_phi.push_back(Z2->phi());
    Zs_mass.push_back(Z2->mass());
    Zs_pdgId.push_back(Z2->pdgId());
  }

  Z1bs.clear();
  Z2bs.clear();

  std::vector<const reco::Candidate*> finalbs;
  std::vector<const reco::Candidate*> finalqs;
  std::vector<double> b_E;
  std::vector<double> b_pT;
  std::vector<double> b_eta;
  std::vector<double> b_phi;
  std::vector<double> b_mass;
  std::vector<double> q_E;
  std::vector<double> q_pT;
  std::vector<double> q_eta;
  std::vector<double> q_phi;
  std::vector<double> q_mass;
  std::vector<int> q_pdgId;
  const reco::Candidate* b0;
  const reco::Candidate* q0;

  if (debug) std::cout << "Start Z loop" << std::endl;

  for ( unsigned int iZ = 0; iZ < nZ; iZ++ ) {

    reco::GenParticleRef iZed = ( iZ == 0 ? Z1 : Z2 );

    b_E.clear();
    b_pT.clear();
    b_eta.clear();
    b_phi.clear();
    b_mass.clear();
    q_E.clear();
    q_pT.clear();
    q_eta.clear();
    q_phi.clear();
    q_mass.clear();
    q_pdgId.clear();

    // check initial b daughters of Z
    for (unsigned int i = 0; i < iZed->numberOfDaughters(); i++) {
      if( iZed->daughter(i)->status() == 23 && abs(iZed->daughter(i)->pdgId()) == 5) {
	b_E.push_back(iZed->daughter(i)->energy());
	b_pT.push_back(iZed->daughter(i)->pt());
	b_eta.push_back(iZed->daughter(i)->eta());
	b_phi.push_back(iZed->daughter(i)->phi());
	b_mass.push_back(iZed->daughter(i)->mass());
      }
      if( iZed->daughter(i)->status() == 23 && abs(iZed->daughter(i)->pdgId()) <= 6 && abs(iZed->daughter(i)->pdgId()) >= 1 )   {
	q_E.push_back(iZed->daughter(i)->energy());
        q_pT.push_back(iZed->daughter(i)->pt());
        q_eta.push_back(iZed->daughter(i)->eta());
        q_phi.push_back(iZed->daughter(i)->phi());
	q_mass.push_back(iZed->daughter(i)->mass());
	q_pdgId.push_back(iZed->daughter(i)->pdgId());
      }
    }

    Zs_orig_Bs_E.push_back(b_E);
    Zs_orig_Bs_pT.push_back(b_pT);
    Zs_orig_Bs_eta.push_back(b_eta);
    Zs_orig_Bs_phi.push_back(b_phi);
    Zs_orig_Bs_mass.push_back(b_mass);
    Zs_orig_Qs_E.push_back(q_E);
    Zs_orig_Qs_pT.push_back(q_pT);
    Zs_orig_Qs_eta.push_back(q_eta);
    Zs_orig_Qs_phi.push_back(q_phi);
    Zs_orig_Qs_mass.push_back(q_mass);
    Zs_orig_Qs_pdgId.push_back(q_pdgId);

    // check end-state b quarks
    finalbs.clear();
    listBDaughters(iZed, finalbs);
    if( iZ == 0 ) {
      listBDaughters(iZed, Z1bs);
    } else if( iZ == 1 ) {
      listBDaughters(iZed, Z2bs);
    }

    if (debug) std::cout << "fillObj EvtID " << eventId << " status " << iZed->status() << " particle ID " << iZed->pdgId() << " #Bs: " << finalbs.size() << std::endl;


    b_E.clear();
    b_pT.clear();
    b_eta.clear();
    b_phi.clear();
    b_mass.clear();
    
    for( unsigned int ib = 0; ib < finalbs.size(); ib++ ) {
      b0 = finalbs[ib];
      
      b_E.push_back(b0->energy());
      b_pT.push_back(b0->pt());
      b_eta.push_back(b0->eta());
      b_phi.push_back(b0->phi());
      b_mass.push_back(b0->mass());
    }

    Zs_final_Bs_E.push_back(b_E);
    Zs_final_Bs_pT.push_back(b_pT);
    Zs_final_Bs_eta.push_back(b_eta);
    Zs_final_Bs_phi.push_back(b_phi);
    Zs_final_Bs_mass.push_back(b_mass);


    q_E.clear();
    q_pT.clear();
    q_eta.clear();
    q_phi.clear();
    q_mass.clear();
    q_pdgId.clear();

    finalqs.clear();
    listEndQs(iZed, finalqs);

    //std::cout << iZed->pdgId() << " " << finalqs.size() << " " << finalbs.size() << std::endl;
    
    for( unsigned int iQ = 0; iQ < finalqs.size(); iQ++ ) {
      q0 = finalqs[iQ];

      q_E.push_back(q0->energy());
      q_pT.push_back(q0->pt());
      q_eta.push_back(q0->eta());
      q_phi.push_back(q0->phi());
      q_mass.push_back(q0->mass());
      q_pdgId.push_back(q0->pdgId());
    }

    Zs_final_Qs_E.push_back(q_E);
    Zs_final_Qs_pT.push_back(q_pT);
    Zs_final_Qs_eta.push_back(q_eta);
    Zs_final_Qs_phi.push_back(q_phi);
    Zs_final_Qs_mass.push_back(q_mass);
    Zs_final_Qs_pdgId.push_back(q_pdgId);


  }

  std::vector<const reco::Candidate*> AllTruthBs;
  std::vector<const reco::Candidate*> NonZTruthBs;

  finalbs.clear();
  if (nZ >= 1) listBDaughters(Z1, finalbs);
  if (nZ >= 2) listBDaughters(Z2, finalbs);
  //printProgeny(initProtons[0]);
  listBDaughters(initProtons[0], AllTruthBs);
  for( unsigned int iB = 0; iB < AllTruthBs.size(); iB++ ) {
    const reco::Candidate* thisb = AllTruthBs[iB];
    //std::cout << "TB" << iB << " " << thisb->pt() << " " << thisb->eta() << " " << thisb->mass() << std::endl;

    AllTruthBs_energy.push_back(thisb->energy());
    AllTruthBs_pt.push_back(thisb->pt());
    AllTruthBs_eta.push_back(thisb->eta());
    AllTruthBs_phi.push_back(thisb->phi());
    AllTruthBs_mass.push_back(thisb->mass());

    bool matched = false;
    for( unsigned int iC = 0; iC < finalbs.size(); iC++ ) {
      const reco::Candidate* thatb = finalbs[iC];
      //std::cout << (thisb->pt() == thatb->pt()) << " " << (thisb->eta() == thatb->eta()) << " " << (thisb->phi() == thatb->phi()) << std::endl;
      if ((thisb->pt() == thatb->pt()) && (thisb->eta() == thatb->eta()) && (thisb->phi() == thatb->phi())) matched = true;
    }
    
    if (!matched) {
      NonZTruthBs_energy.push_back(thisb->energy());
      NonZTruthBs_pt.push_back(thisb->pt());
      NonZTruthBs_eta.push_back(thisb->eta());
      NonZTruthBs_phi.push_back(thisb->phi());
      NonZTruthBs_mass.push_back(thisb->mass());
    } else if (matched) {
      ZTruthBs_energy.push_back(thisb->energy());
      ZTruthBs_pt.push_back(thisb->pt());
      ZTruthBs_eta.push_back(thisb->eta());
      ZTruthBs_phi.push_back(thisb->phi());
      ZTruthBs_mass.push_back(thisb->mass());
    }

  }

  if (debug) std::cout << ZTruthBs_pt.size() << " " << NonZTruthBs_pt.size() << " " << AllTruthBs_pt.size() << std::endl;

  if (debug) std::cout << "Done filling obj vectors" << std::endl;
}

