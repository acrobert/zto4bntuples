// --------------------------------------------
//
// Package:    Zto4Bntuples
// Class:      ntuplizer
// 
//
// Author: Andrew C. Roberts
// Started 2022/10/4
// Last Updated 2022/10/4
//
// --------------------------------------------

#include "Zto4Bntuples/ntuples/interface/ntuplizer.h"
#include <stdlib.h>

std::vector<std::vector<float>> duo_pft_img;
std::vector<std::vector<float>> trio_pft_img;
std::vector<std::vector<float>> quad_pft_img;

// initialize branches
void ntuplizer::branchesPFTImages ( TTree* tree, edm::Service<TFileService> &fs )
{

  EvtTree->Branch("duo_pft_img", &duo_pft_img);
  EvtTree->Branch("trio_pft_img", &trio_pft_img);
  EvtTree->Branch("quad_pft_img", &quad_pft_img);

}

// fill b group object histograms
void ntuplizer::fillPFTImages(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{

  std::vector<float> pft_img;

  edm::Handle<PFCollection> PFCandsH;
  iEvent.getByToken(pfCollectionT_, PFCandsH);

  edm::ESHandle<CaloGeometry> caloGeomH;
  iSetup.get<CaloGeometryRecord>().get(caloGeomH);
  const CaloGeometry* caloGeom = caloGeomH.product();

  duo_pft_img.clear();  
  trio_pft_img.clear();  
  quad_pft_img.clear();

  float eta, phi;
  int ieta, iphi, ieta_crop, iphi_crop, ieta_shift, iphi_shift, idx;
  if( save_this_event ) {
    for( unsigned int iG = 0; iG < img_type.size(); iG++ ) {
      float ctr_eta = img_eta[iG];
      float ctr_phi = img_phi[iG];

      pft_img.assign( crop_size*crop_size, 0. );
      DetId ctrId( spr::findDetIdECAL( caloGeom, ctr_eta, ctr_phi, false ) );
      if ( ctrId.subdetId() != EcalBarrel ) continue;
      EBDetId ctrEBId( ctrId );
      
      ieta_shift = ctrEBId.ieta() - crop_shift;
      iphi_shift = ctrEBId.iphi() - crop_shift;

      for ( PFCollection::const_iterator iPFC = PFCandsH->begin();
	   iPFC != PFCandsH->end(); ++iPFC ) {

	const reco::Track* thisTrk = iPFC->bestTrack();
	if(!thisTrk) continue;
	
	eta = iPFC->eta();
	phi = iPFC->phi();
	if ( std::abs(eta) > 1.5 ) continue;

	DetId trkId( spr::findDetIdECAL( caloGeom, eta, phi, false ) );
	if ( trkId.subdetId() != EcalBarrel ) continue;
	EBDetId trkEBId( trkId );

	float trkPt = thisTrk->pt();
	ieta = trkEBId.ieta() > 0 ? trkEBId.ieta()-1 : trkEBId.ieta(); // [-85,...,-1,0,...,84]
	iphi = trkEBId.iphi()-1; // [0,...,359]
	ieta += EBDetId::MAX_IETA; // [0,...,169]
	
	ieta_crop = ieta - ieta_shift;
	iphi_crop = iphi - iphi_shift;
	if ( iphi_crop >= EBDetId::MAX_IPHI ) iphi_crop = iphi_crop - EBDetId::MAX_IPHI; // get wrap-around hits                                                                                         
	if ( iphi_crop < 0 ) iphi_crop = iphi_crop + EBDetId::MAX_IPHI; // get wrap-around hits                                                                                                          
	
	// select image
	if ( ieta_crop < 0 || ieta_crop > crop_size-1 ) continue;
	if ( iphi_crop < 0 || iphi_crop > crop_size-1 ) continue;

	idx = ieta_crop*crop_size + iphi_crop;
	pft_img[idx] = trkPt;

      }

      if( img_type[iG] == 2 ) duo_pft_img.push_back(pft_img);
      if( img_type[iG] == 3 ) trio_pft_img.push_back(pft_img);
      if( img_type[iG] == 4 ) quad_pft_img.push_back(pft_img);

    }
  }
}

