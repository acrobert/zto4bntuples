export SCRAM_ARCH=slc7_amd64_gcc700 # or any compatible arch
#source /cvmfs/sft.cern.ch/lcg/views/LCG_97/x86_64-centos7-gcc8-opt/setup.sh
cmsenv
voms-proxy-init -voms cms
scram b -j 8
source /cvmfs/cms.cern.ch/common/crab-setup.sh
