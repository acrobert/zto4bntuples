import os

cfg='Zto4Bntuples/ntuples/python/ntuplizer_cfg.py'

# MINIAOD
#inputFiles_ = '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v1/2430000/086B8995-7BC6-E14F-9033-56E0675E9037.root'
#inputFiles_ = '/store/mc/RunIISummer20UL18MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/40000/018BD1A4-4C9A-A247-BD30-71DB1A95B7C8.root'
#inputFiles_ = '/store/mc/RunIISummer20UL18MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/40000/01BF27C6-3790-BA49-9AB6-EBC044769933.root'
#inputFiles_ = '/store/mc/RunIISummer20UL18MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/40000/021883C4-65D7-D54A-A353-F900007CB34E.root'
#inputFiles_ = '/store/mc/RunIISummer20UL18MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/40000/02518853-F5A3-5B42-8806-1EB9CE072DE4.root'
#inputFiles_ = '/store/mc/RunIISummer20UL18MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/40000/02C32A45-C597-F545-AEAE-B8C93A12607A.root'
#inputFiles_ = '/store/mc/RunIISummer20UL18MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/40000/03A2170F-5F1A-8B45-B903-6E2E68090E9C.root'

inputFiles_ = [
    #'/store/mc/RunIISummer20UL18MiniAODv2/WZTo1L1Nu2Q_4f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/2830000/028856DB-9B84-1F4E-A748-107319994E1A.root'
  
    '/store/mc/RunIISummer20UL18MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/120000/4E7C6F3B-8478-EE4B-84EF-C035CD5D2A99.root'
    ,'/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120002/5934A0D0-5A92-3743-AE58-4FED46E81CD6.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120002/AF721DD9-D446-5B4A-BE10-B8F79436772A.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120002/BBD03918-2751-5943-8FAD-B1CC06ECD0FD.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120002/B5612F9F-B4C7-5646-A100-327AD2921546.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120002/53F84C10-132A-0F46-920C-631062A4B166.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120002/65242315-62D7-B148-A0DA-E8A13E7B4F06.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120002/15F2C602-7DC1-9A47-8874-8F2944D69B76.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120002/3F15C4A9-6BED-014A-967B-D9BECA90C21F.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120002/3B74A054-799F-D24F-AADF-05A0E29FC754.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120002/AC42CE19-2814-9147-B797-4A0347206C16.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120002/73FB94C2-3838-BD4E-B3CF-BCE6CE3A53CC.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120002/9192BD69-A506-BB4B-A4B8-DBE2ADEB12E0.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120002/85728D77-E508-DD44-BCEB-5EF685BB6D0F.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120002/771923E6-3A77-FB41-B9C8-241F211A0FE0.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120002/5C9FF82E-E1D4-9D42-AA63-F2D39F511F0E.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120002/FEB21DA2-4E20-B74B-8A5E-7987B8235B28.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120002/B9B7FAC1-6A8B-D94C-9CE4-809EBCC5677B.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120002/05B8B8BA-EEC0-2241-86FA-0C8920F3376C.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120002/1E811BE7-C9A2-4C45-8D67-E31A147726DC.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120000/839F6BCB-B254-A043-9B64-2073360FEAB5.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120000/BECCC902-5CEB-4847-8809-B86E5958B775.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120000/173F3D1E-1BD3-FE44-A0EA-4712AC1BE083.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120000/B737E6B5-D0A4-A444-80B9-FC2B3400F0F8.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120000/8E9B7180-EAB2-2A42-B30D-A5455DB15C9F.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120000/FCF52EC9-2435-014A-9FDB-4DD429BF7F60.root'
    , '/store/mc/RunIISummer20UL17MiniAODv2/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v2/120000/C7153D76-622D-C744-9D93-626062F46EFA.root'
]

#maxEvents_=-1
maxEvents_=5000
skipEvents_=0
instr = ' '.join([ 'inputFiles='+s for s in inputFiles_ ])
cmd="cmsRun %s %s maxEvents=%d skipEvents=%d"%(cfg,instr,maxEvents_,skipEvents_)
print ('%s'%cmd)
os.system(cmd)

