import os

cfg='Zto4Bntuples/ntuples/python/ntuplizer_cfg.py'

# MINIAOD
#inputFiles_ = '/store/mc/RunIISummer20UL17MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v1/2430000/086B8995-7BC6-E14F-9033-56E0675E9037.root'
#inputFiles_ = '/store/mc/RunIISummer20UL18MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/40000/018BD1A4-4C9A-A247-BD30-71DB1A95B7C8.root'
#inputFiles_ = '/store/mc/RunIISummer20UL18MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/40000/01BF27C6-3790-BA49-9AB6-EBC044769933.root'
#inputFiles_ = '/store/mc/RunIISummer20UL18MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/40000/021883C4-65D7-D54A-A353-F900007CB34E.root'
#inputFiles_ = '/store/mc/RunIISummer20UL18MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/40000/02518853-F5A3-5B42-8806-1EB9CE072DE4.root'
#inputFiles_ = '/store/mc/RunIISummer20UL18MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/40000/02C32A45-C597-F545-AEAE-B8C93A12607A.root'
#inputFiles_ = '/store/mc/RunIISummer20UL18MiniAODv2/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/40000/03A2170F-5F1A-8B45-B903-6E2E68090E9C.root'

inputFiles_ = [
    '/store/data/Run2018A/JetHT/MINIAOD/UL2018_MiniAODv2-v1/260000/00B87525-94D1-C741-9B03-00528106D15A.root'
    , '/store/data/Run2018A/JetHT/MINIAOD/UL2018_MiniAODv2-v1/260000/00F0369F-B9FA-5643-B0F4-F286B80B30B5.root'
    , '/store/data/Run2018A/JetHT/MINIAOD/UL2018_MiniAODv2-v1/260000/0173DB9C-3E46-4741-B80B-9671E9E917CA.root'

]

# AOD
#inputFiles_ = '/store/mc/RunIISummer20UL17RECO/ZZTo4B01j_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8/AODSIM/106X_mc2017_realistic_v6-v1/2520000/000E2D11-E084-CF41-8315-AAD7414F1DCF.root'

#maxEvents_=-1
maxEvents_=5000
skipEvents_=0
instr = ' '.join([ 'inputFiles='+s for s in inputFiles_ ])
cmd="cmsRun %s %s maxEvents=%d skipEvents=%d"%(cfg,instr,maxEvents_,skipEvents_)
print ('%s'%cmd)
os.system(cmd)

